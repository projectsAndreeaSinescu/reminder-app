# Reminder App
Este o aplicatie mobile (iOS) dezvoltata cu Swift, creata in cadrul practicii de la HyperSense Software. In aceasta, se pot crea, edita si sterge
evenimente si se poate seta un reminder, primind mai apoi o
notificare conform acestuia. Evenimentele contin un nume, o
descriere, o fotografie (optionala), locatia si data la care se desfasoara.

Am reusit sa invat sa creez o aplicatie mobile (iOS) de la zero, sa creez animatii interesante care sa aduca produsul final la viata si sa il faca mai interesant,
sa stilizez ecranele si sa le fac responsive indiferent de dimensiunea ecranului sau tipul dispozitivului. Am avut parte de un proces complet de mentorat si pot 
spune ca am reusit sa invat foarte repede.
