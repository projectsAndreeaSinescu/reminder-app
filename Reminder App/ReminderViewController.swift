//
//  HomeViewController.swift
//  Remainder App
//
//  Created by Andreea Sinescu on 19/07/2018.
//  Copyright © 2018 Andreea Sinescu. All rights reserved.
//

import UIKit
import UserNotifications

class ReminderViewController: UIViewController {

    @IBOutlet weak var viewCreateEvent: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewAnimation: UIView!
    @IBOutlet weak var createEventButton: UIButton!
    @IBOutlet weak var cellAnimation: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var monthTextField: HSTextField!
    @IBOutlet weak var dayTextField: UITextField!
    @IBOutlet weak var yearTextField: UITextField!
    @IBOutlet weak var imageViewBackground: UIImageView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var leftConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintSearch: NSLayoutConstraint!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var bottomTableConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageAnimation: UIImageView!
    @IBOutlet weak var animationViewCell : UIView!
    @IBOutlet weak var logoutButton: UIButton!
    
    var listEvent = [Event]()
    var filterListEvent = [Event]()
    var selectedIndexPath: IndexPath!
    var added = false
    var cancel = false
    var edited = false
    var imagesAnimation = [UIImage]()
    var selected = false
    var lines: Int?
    
    var months: [(String, Int)] = [("January", 31), ("February", 28), ("March", 31), ("April", 30), ("May", 31), ("June", 30), ("July", 31), ("August", 31), ("September", 30), ("October", 31), ("November", 30), ("December", 31)]
    
    var years: [Int] = []
    
    let dayPicker = UIPickerView()
    let monthPicker = UIPickerView()
    let yearPicker = UIPickerView()
    var monthSelected: Int?
    var yearSelected: Int?
    var daySelected: Int?
    
    static func createViewController() -> ReminderViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ReminderViewController") as! ReminderViewController
        return controller
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        animationViewCell.alpha = 0
        
        dayPicker.delegate = self
        dayPicker.dataSource = self
        monthPicker.delegate = self
        monthPicker.dataSource = self
        yearPicker.delegate = self
        yearPicker.dataSource = self
        
        dayTextField.inputView = dayPicker
        monthTextField.inputView = monthPicker
        yearTextField.inputView = yearPicker
        dayTextField.isEnabled = false
        monthTextField.isEnabled = false
        
        let constant = tableView.frame.origin.y - view.frame.size.height + 20
        
        if constant + 500 < -filterButton.frame.size.height - 10 {
            bottomTableConstraint.constant = constant + 510
        } else if constant + 400 < -filterButton.frame.size.height - 10 {
            bottomTableConstraint.constant = constant + 400
        } else if constant + 300 < -filterButton.frame.size.height - 10 {
            bottomTableConstraint.constant = constant + 300
        } else {
            bottomTableConstraint.constant = constant + 200
        }
        
        for i in 2018...2200 {
            years.append(i)
        }
        
        WebService.sharedInstance.getReminders { (response) in
            guard response.isSuccess else {
                return
            }
//            self.listEvent = response.parsedResult as! [Event]
        }
        listEvent = HSCoreDataController.sharedInstance.retrieveEvents()
        listEvent = listEvent.reversed()
        viewCreateEvent.layer.cornerRadius = 28
        viewAnimation.alpha = 0
        viewAnimation.layer.cornerRadius = 28
        searchView.alpha = 0
        searchView.layer.cornerRadius = 8
        for i in 1...30 {
            imagesAnimation.append(UIImage(named: "\(i)")!)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if added == true {
            animationFinishCreateEvent()
        } else if cancel == true || edited == true {
            cancel = false
            edited = false
            animationFinishCreateEvent()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        leftConstraint.constant = filterButton.frame.origin.x
        heightConstraintSearch.constant = filterButton.frame.height
        self.view.layoutIfNeeded()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Actions
    @IBAction func logout() {
        WebService.sharedInstance.logout { (response) in
            guard response.isSuccess else {
                self.showAlert(title: "Atention", message: "\(response.errorMessage!)")
                return
            }
            let controller = LogInViewController.createViewController()
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    @IBAction func createEvent() {
        if self.selected == true {
            self.selected = false
            self.filterButton.alpha = 0
            self.containtAnimate()
            animationTableView()
        }
        if let _ = selectedIndexPath {
            let indexPath = selectedIndexPath
            selectedIndexPath = nil
            self.tableView.reloadRows(at: [indexPath!], with: .automatic)
        }
        animationCreateEvent()
    }
    
    @IBAction func editEvent() {
        let draw = tableView.rectForRow(at: selectedIndexPath!)
        cellAnimation.frame.size.height = draw.size.height - 6
        cellAnimation.frame.size.width = draw.size.width
        cellAnimation.frame.origin.x = draw.origin.x
        cellAnimation.frame.origin.y = draw.origin.y - tableView.contentOffset.y + tableView.frame.origin.y + 3
        cellAnimation.layer.cornerRadius = 39
        cellAnimation.alpha = 1
        editAnimation()
    }
    
    @IBAction func deleteEvent() {
        removeNotification(uid: listEvent[selectedIndexPath.row].uid!)
        HSCoreDataController.sharedInstance.deleteManagedObject(listEvent[selectedIndexPath.row])
        HSCoreDataController.sharedInstance.saveContext()
        let _ = listEvent.remove(at: selectedIndexPath!.row)
        tableView.deleteRows(at: [selectedIndexPath!], with: .fade)
        selectedIndexPath = nil
    }
    
    @IBAction func filterAnimation() {
        if filterListEvent.isEmpty && listEvent.isEmpty {
            self.showAlert(title: "Atention", message: "Add an event before.")
            return
        }
        backButton.isHidden = false
        if let _ = selectedIndexPath {
            let indexPath = selectedIndexPath
            selectedIndexPath = nil
            self.tableView.reloadRows(at: [indexPath!], with: .automatic)
        }
        if selected == false {
            selected = true
            let constant = tableView.frame.origin.y - view.frame.size.height + 20
            
            if constant + 500 < -searchView.frame.size.height - 20 {
                bottomTableConstraint.constant = constant + 510
            } else if constant + 400 < -searchView.frame.size.height - 20 {
                bottomTableConstraint.constant = constant + 400
            } else if constant + 300 < -searchView.frame.size.height - 20 {
                bottomTableConstraint.constant = constant + 300
            } else {
                bottomTableConstraint.constant = constant + 200
            }
            filterButton.alpha = 0
            imageView.alpha = 1
            imageView.image = imagesAnimation.last
            imageView.animationImages = imagesAnimation
            imageView.animationDuration = 0.6
            imageView.animationRepeatCount = 1
            imageView.startAnimating()
            containerAnimate()
        } else {
            if yearSelected == nil && searchTextField.text! == ""{
                showAlert(title: "Whoops", message: "Complete what do you want to find")
                return
            }
            if !filterListEvent.isEmpty
            {
                listEvent = filterListEvent
            }
            filterListEvent = listEvent
            listEvent = listEvent.filter { (event) -> Bool in
                if let _ = searchTextField.text, let _ = yearSelected, let _ = monthSelected, let _ = daySelected, let _ = event.dateEvent {
                    let components = Calendar.current.dateComponents([.year, .month, .day], from: event.dateEvent!)
                    if searchTextField.text! != "" {
                        return (event.nameEvent!.lowercased().contains(searchTextField.text!.lowercased()) || event.descriptionEvent!.lowercased().contains(searchTextField.text!.lowercased())) && components.year == years[yearSelected!] && components.month == monthSelected! + 1 && components.day == daySelected!
                    } else {
                        return components.year == years[yearSelected!] && components.month == monthSelected! + 1 && components.day == daySelected!
                    }
                } else if let _ = searchTextField.text, let _ = yearSelected, let _ = monthSelected, let _ = event.dateEvent {
                    let components = Calendar.current.dateComponents([.year, .month], from: event.dateEvent!)
                    if searchTextField.text! != "" {
                        return (event.nameEvent!.lowercased().contains(searchTextField.text!.lowercased()) || event.descriptionEvent!.lowercased().contains(searchTextField.text!.lowercased())) && components.year == years[yearSelected!] && components.month == monthSelected! + 1
                    } else {
                        return components.year == years[yearSelected!] && components.month == monthSelected! + 1
                    }
                } else if let _ = searchTextField.text, let _ = yearSelected, let _ = event.dateEvent {
                    let components = Calendar.current.dateComponents([.year, .month], from: event.dateEvent!)
                    if searchTextField.text! != "" {
                        return (event.nameEvent!.lowercased().contains(searchTextField.text!.lowercased()) || event.descriptionEvent!.lowercased().contains(searchTextField.text!.lowercased())) && components.year == years[yearSelected!]
                    } else {
                        return components.year == years[yearSelected!]
                    }
                } else if let text = searchTextField.text {
                    if text == "" {
                        return false
                    }
                    return event.nameEvent!.lowercased().contains(text.lowercased()) || event.descriptionEvent!.lowercased().contains(text.lowercased()) || false
                } else {
                    return false
                }
            }
            if !listEvent.isEmpty {
                tableView.reloadData()
                tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: false)
            } else {
                self.showAlert(title: "Atention", message: "No item found")
            }
            selected = false
            filterButton.alpha = 0
            animationTableView()
            containtAnimate()
        }
    }
    
    @IBAction func cancelFilter() {
        yearSelected = nil
        monthSelected = nil
        daySelected = nil
        yearTextField.text = ""
        dayTextField.text = ""
        monthTextField.text = ""
        searchTextField.text = ""
        backButton.isHidden = true
        monthTextField.isEnabled = false
        dayTextField.isEnabled = false
        yearPicker.selectRow(0, inComponent: 0, animated: false)
        monthPicker.selectRow(0, inComponent: 0, animated: false)
        dayPicker.selectRow(0, inComponent: 0, animated: false)
        if !filterListEvent.isEmpty {
            listEvent = filterListEvent
            filterListEvent = [Event]()
            tableView.reloadData()
            tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: false)
        } else {
            tableView.reloadData()
            tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: false)
        }
        if selected == true {
            selected = false
            containtAnimate()
            animationTableView()
        }
    }
    
    //MARK:- Filter
    func filter() {
        listEvent = filterListEvent
        listEvent = listEvent.filter { (event) -> Bool in
            if let _ = searchTextField.text, let _ = yearSelected, let _ = monthSelected, let _ = daySelected, let _ = event.dateEvent {
                let components = Calendar.current.dateComponents([.year, .month, .day], from: event.dateEvent!)
                if searchTextField.text! != "" {
                    return (event.nameEvent!.lowercased().contains(searchTextField.text!.lowercased()) || event.descriptionEvent!.lowercased().contains(searchTextField.text!.lowercased())) && components.year == years[yearSelected!] && components.month == monthSelected! + 1 && components.day == daySelected!
                } else {
                    return components.year == years[yearSelected!] && components.month == monthSelected! + 1 && components.day == daySelected!
                }
            } else if let _ = searchTextField.text, let _ = yearSelected, let _ = monthSelected, let _ = event.dateEvent {
                let components = Calendar.current.dateComponents([.year, .month], from: event.dateEvent!)
                if searchTextField.text! != "" {
                    return (event.nameEvent!.lowercased().contains(searchTextField.text!.lowercased()) || event.descriptionEvent!.lowercased().contains(searchTextField.text!.lowercased())) && components.year == years[yearSelected!] && components.month == monthSelected! + 1
                } else {
                    return components.year == years[yearSelected!] && components.month == monthSelected! + 1
                }
            } else if let _ = searchTextField.text, let _ = yearSelected, let _ = event.dateEvent {
                let components = Calendar.current.dateComponents([.year, .month], from: event.dateEvent!)
                if searchTextField.text! != "" {
                    return (event.nameEvent!.lowercased().contains(searchTextField.text!.lowercased()) || event.descriptionEvent!.lowercased().contains(searchTextField.text!.lowercased())) && components.year == years[yearSelected!]
                } else {
                    return components.year == years[yearSelected!]
                }
            } else if let text = searchTextField.text {
                if text == "" {
                    return false
                }
                return event.nameEvent!.lowercased().contains(text.lowercased()) || event.descriptionEvent!.lowercased().contains(text.lowercased()) || false
            } else {
                return false
            }
        }
        if !listEvent.isEmpty {
            tableView.reloadData()
        }
    }
    
    //MARK:- Notification functions
    func removeNotification(uid: String) {
        let center = UNUserNotificationCenter.current()
        center.removePendingNotificationRequests(withIdentifiers: [uid])
    }
    
    //MARK:- Animations
    func animationCreateEvent() {
        tableView.alpha = 0
        viewCreateEvent.backgroundColor = UIColor.clear
        viewAnimation.alpha = 1
        self.createEventButton.setImage(#imageLiteral(resourceName: "okButton"), for: .normal)
        self.createEventButton.setBackgroundImage(nil, for: .normal)
        self.createEventButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.6,
                       animations: {
                        self.logoutButton.alpha = 0
                        self.viewAnimation.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.31)
                        self.heightConstraint.constant = 435
                        self.view.layoutIfNeeded()
        }) { (_) in
            let controller = CreateEventViewController.createViewController(delegate: self, for: nil)
            self.present(controller, animated: false, completion: {
                self.createEventButton.setImage(#imageLiteral(resourceName: "plus"), for: .normal)
                self.createEventButton.setBackgroundImage(#imageLiteral(resourceName: "Oval"), for: .normal)
                self.createEventButton.imageEdgeInsets = UIEdgeInsets(top: 6, left: 0, bottom: 0, right: 0)
                self.tableView.alpha = 1
                self.viewCreateEvent.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.07)
                self.viewAnimation.alpha = 0
                self.viewAnimation.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.07)
                self.heightConstraint.constant = 65
            })
        }
    }
    
    func animationFinishCreateEvent() {
        tableView.alpha = 0
        viewCreateEvent.backgroundColor = UIColor.clear
        viewAnimation.alpha = 1
        viewAnimation.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.31)
        var draw = CGRect()
        if added == true && filterListEvent.isEmpty {
            if listEvent[0].image != nil {
                imageAnimation.image = UIImage(data: listEvent[0].image!)
            } else {
                imageAnimation.image = #imageLiteral(resourceName: "pictureImage")
            }
            draw = tableView.rectForRow(at: IndexPath(row: 0, section: 0))
            imageAnimation.frame.origin.x = view.frame.size.width - 20 - imageAnimation.frame.size.width
            imageAnimation.frame.origin.y = 226
            imageAnimation.alpha = 1
        }
        self.heightConstraint.constant = 435
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.6,
                       animations: {
                        if self.added == true && self.filterListEvent.isEmpty {
                            self.imageAnimation.frame.size.width = 100
                            self.imageAnimation.frame.size.height = 98
                            self.imageAnimation.frame.origin.x = 0
                            self.imageAnimation.frame.origin.y = draw.origin.y + self.tableView.frame.origin.y + 4
                        }
                        self.logoutButton.alpha = 1
                        self.viewAnimation.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.07)
                        self.heightConstraint.constant = 65
                        self.view.layoutIfNeeded()
        }) { (_) in
            self.tableView.alpha = 1
            self.viewCreateEvent.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.07)
            self.viewAnimation.alpha = 0
            if self.added == true && self.filterListEvent.isEmpty {
                let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? EventTableViewCell
                cell!.nameLabel.alpha = 0
                cell!.descriptionLabel.alpha = 0
                cell!.hourLabel.alpha = 0
                cell!.dateLabel.alpha = 0
                cell!.locationLabel.alpha = 0
                cell!.locationButton.alpha = 0
                cell!.containerView.backgroundColor = UIColor.clear
                self.imageAnimation.alpha = 0
                self.cellBigAnimation()
            }
        }
    }
    
    func animationTableView() {
        UIView.animate(withDuration: 0.8) {
            let constant = self.tableView.frame.origin.y - self.view.frame.size.height + 20
            
            if constant + 500 < -self.filterButton.frame.size.height - 10 {
                self.bottomTableConstraint.constant = constant + 510
            } else if constant + 400 < -self.filterButton.frame.size.height - 10 {
                self.bottomTableConstraint.constant = constant + 400
            } else if constant + 300 < -self.filterButton.frame.size.height - 10 {
                self.bottomTableConstraint.constant = constant + 300
            } else {
                self.bottomTableConstraint.constant = constant + 200
            }
            self.view.layoutIfNeeded()
        }
    }
    
    func containerAnimate() {
        UIView.animate(withDuration: 0.4, animations: {
            self.leftConstraint.constant = 20
            self.heightConstraintSearch.constant = 110
            self.view.layoutIfNeeded()
        }, completion: { _ in
            self.containtAnimate()
        })
    }
    
    func containtAnimate() {
        UIView.animate(withDuration: 0.2, animations: {
            self.searchView.alpha = 1 - self.searchView.alpha
            self.view.layoutIfNeeded()
        }) { _ in
            if self.searchView.alpha == 0 {
                self.containerAnimateReverse()
            } else {
                self.filterButton.alpha = 1
            }
            
        }
    }
    
    func containerAnimateReverse() {
        imageView.alpha = 1
        imageView.image = imagesAnimation.first
        imageView.animationImages = imagesAnimation.reversed()
        imageView.animationDuration = 0.6
        imageView.animationRepeatCount = 1
        imageView.startAnimating()
        UIView.animate(withDuration: 0.4, animations: {
            self.leftConstraint.constant = self.filterButton.frame.origin.x
            self.heightConstraintSearch.constant = self.filterButton.frame.height
            self.view.layoutIfNeeded()
        }) { _ in
            self.view.layoutIfNeeded()
            self.filterButton.alpha = 1
        }
    }
    
    func editAnimation() {
        viewCreateEvent.alpha = 0
        tableView.alpha = 0
        let event = listEvent[selectedIndexPath!.row]
        let existingLines = event.descriptionEvent!.components(separatedBy: CharacterSet.newlines)
        let numberOfChars = event.descriptionEvent!.count
        var height = 435.0
        if existingLines.count == 2 || (numberOfChars < 75 && numberOfChars >= 38) {
            height = 454
        } else if existingLines.count == 3 || (numberOfChars < 113 && numberOfChars >= 75){
            height = 473
            
        } else if existingLines.count == 4 || (numberOfChars <= 120 && numberOfChars >= 113){
            height = 492.5
        }
        if self.selected == true {
            filterButton.alpha = 0
        }
        UIView.animate(withDuration: 0.6, animations: {
            self.logoutButton.alpha = 0
            self.cellAnimation.frame.origin.y = self.viewCreateEvent.frame.origin.y
            self.cellAnimation.layer.cornerRadius = 28
            self.cellAnimation.frame.size.height = CGFloat(height)
            self.view.layoutIfNeeded()
            if self.selected == true {
                self.searchView.alpha = 0
                self.imageViewBackground.alpha = 0
                self.imageView.alpha = 0
            }
        }) { _ in
            let controller = CreateEventViewController.createViewController(delegate: self, for: self.listEvent[self.selectedIndexPath!.row])
            self.present(controller, animated: false, completion: {
                self.cellAnimation.alpha = 0
                self.viewCreateEvent.alpha = 1
                self.tableView.alpha = 1
                let indexPath = self.selectedIndexPath
                self.selectedIndexPath = nil
                self.tableView.reloadRows(at: [indexPath!], with: .automatic)
                if self.selected == true {
                    self.selected = false
                    self.searchView.alpha = 1
                    self.imageViewBackground.alpha = 1
                    self.imageView.alpha = 1
                    self.containtAnimate()
                    self.animationTableView()
                }
            })

        }
    }
    
    func cellBigAnimation() {
        animationViewCell.frame.size.width = imageAnimation.frame.size.width
        animationViewCell.frame.size.height = 100
        animationViewCell.frame.origin.x = 0
        animationViewCell.frame.origin.y = imageAnimation.frame.origin.y - 1
        tableView.allowsSelection = false
        animationViewCell.layer.cornerRadius = 39
        view.layoutIfNeeded()
        UIView.animate(withDuration: 0.5, animations: {
            self.animationViewCell.alpha = 1
            self.animationViewCell.frame.size.width = self.view.frame.size.width
        }) {
            (_) in
            let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? EventTableViewCell
            cell!.containerView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.14)
            self.animationViewCell.alpha = 0
            UIView.animate(withDuration: 0.2, animations: {
                cell!.nameLabel.alpha = 1
                cell!.descriptionLabel.alpha = 1
                cell!.hourLabel.alpha = 1
                cell!.dateLabel.alpha = 1
                cell!.locationLabel.alpha = 1
                cell!.locationButton.alpha = 1
            }) {
                _ in
                self.added = false
                self.tableView.allowsSelection = true
            }
        }
    }
    
}

extension ReminderViewController: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if let oldIndexPath = selectedIndexPath
        {
            if oldIndexPath == indexPath {
                selectedIndexPath = nil
                tableView.reloadRows(at: [indexPath], with: .automatic)
            } else {
                selectedIndexPath = indexPath
                tableView.reloadRows(at: [oldIndexPath, indexPath], with: .automatic)
            }
        }
        else
        {
            selectedIndexPath = indexPath
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
        
        if selectedIndexPath != nil, let visibleIndexPaths = tableView.indexPathsForVisibleRows {
            if visibleIndexPaths.first == selectedIndexPath
            {
                self.tableView.scrollToRow(at: self.selectedIndexPath!, at: .top, animated: true)
            } else if visibleIndexPaths.last == selectedIndexPath {
                 self.tableView.scrollToRow(at: self.selectedIndexPath!, at: .bottom, animated: true)
            }
        }
    }
}

extension ReminderViewController: UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listEvent.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kEventTableViewCell, for: indexPath) as! EventTableViewCell
        cell.isLittle = selectedIndexPath != indexPath
        cell.containerView.backgroundColor = selectedIndexPath != indexPath ? UIColor(red: 0, green: 0, blue: 0, alpha: 0.14) : UIColor(red: 0, green: 0, blue: 0, alpha: 0.31)
        let event = listEvent[indexPath.row]
        cell.configure(for: event)
        return cell
    }
}

extension ReminderViewController: CreateEventViewControllerDelegate {
    func createEventViewControllerCancel(_ controller: CreateEventViewController) {
        cancel = true
        self.dismiss(animated: false, completion: nil)
    }
    
    func createEventViewController(_ controller: CreateEventViewController, didFinishAdding event: Event) {
        if (filterListEvent.isEmpty && listEvent.isEmpty) || (filterListEvent.isEmpty && !listEvent.isEmpty){
            listEvent = listEvent.reversed()
            listEvent.append(event)
            listEvent = listEvent.reversed()
            let indexPath = IndexPath(row: 0, section: 0)
            tableView.beginUpdates()
            tableView.insertRows(at: [indexPath], with: .automatic)
            tableView.endUpdates()
            tableView.reloadRows(at: [indexPath], with: .automatic)
            tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: false)
        } else if !filterListEvent.isEmpty {
            filterListEvent = filterListEvent.reversed()
            filterListEvent.append(event)
            filterListEvent = filterListEvent.reversed()
            tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: false)
            filter()
        }
        added = true
        HSCoreDataController.sharedInstance.saveContext()
        self.dismiss(animated: false, completion: nil)
    }
    
    func createEventViewController(_ controller: CreateEventViewController, didFinishEditing event: Event) {
        if let index = listEvent.index(of: event) {
            let indexPath = IndexPath(row: index, section: 0)
            if let cell = tableView.cellForRow(at: indexPath) as? EventTableViewCell {
                cell.configure(for: event)
                if !filterListEvent.isEmpty {
                    filter()
                }
            }
        }
        edited = true
        HSCoreDataController.sharedInstance.saveContext()
        self.dismiss(animated: false, completion: nil)
    }
}

extension ReminderViewController: UIPickerViewDelegate
{
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == yearPicker {
            monthTextField.isEnabled = true
            yearSelected = row
            yearTextField.text = "\(years[row])"
            if let _ = yearSelected, yearSelected! % 4 != 0, let _ = daySelected, daySelected! == 29 {
                daySelected = 28
                dayPicker.selectRow(27, inComponent: 0, animated: false)
                dayTextField.text = "28"
            }
        } else if pickerView == monthPicker {
            dayTextField.isEnabled = true
            if let _ = yearSelected, let _ = daySelected, months[monthSelected!].1 == daySelected, months[row].1 < daySelected! {
                daySelected = months[row].1
                dayTextField.text = "\(daySelected!)"
                dayPicker.selectRow(daySelected! - 1, inComponent: 0, animated: false)
            }
            monthSelected = row
            monthTextField.text = months[row].0
        } else if pickerView == dayPicker {
            daySelected = row + 1
            dayTextField.text = "\(daySelected!)"
        }
    }
}

extension ReminderViewController: UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == yearPicker {
            return years.count
        } else if pickerView == monthPicker {
            return months.count
        } else {
            if let _ = monthSelected {
                if months[monthSelected!].0 == "February" {
                    if years[yearSelected!] % 4 == 0 {
                        return 29
                    } else {
                        return 28
                    }
                }
                return months[monthSelected!].1
            } else {
                return 0
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if yearPicker == pickerView {
            return "\(years[row])"
        } else if monthPicker == pickerView {
            return months[row].0
        } else {
            let number = row + 1
            return "\(number)"
        }
    }
}

extension ReminderViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == dayTextField {
            daySelected = dayPicker.selectedRow(inComponent: 0)
            dayTextField.text = "\(daySelected! + 1)"
        } else if textField == monthTextField {
            monthSelected = monthPicker.selectedRow(inComponent: 0)
            monthTextField.text = "\(months[monthSelected!].0)"
            dayTextField.isEnabled = true
        } else if textField == yearTextField {
            yearSelected = yearPicker.selectedRow(inComponent: 0)
            yearTextField.text = "\(years[yearSelected!])"
            monthTextField.isEnabled = true
        }
    }
}

extension UIViewController
{
    func showAlert(title: String, message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
}


