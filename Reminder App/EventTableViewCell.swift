//
//  EventTableViewCell.swift
//  Remainder App
//
//  Created by Andreea Sinescu on 20/07/2018.
//  Copyright © 2018 Andreea Sinescu. All rights reserved.
//

import UIKit

let kEventTableViewCell = "EventTableViewCell"

class EventTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageEvent: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var bottomLittleConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomBigConstraint: NSLayoutConstraint!
    @IBOutlet weak var locationButton: UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    var isLittle: Bool!
    {
        didSet
        {
            bottomLittleConstraint.priority = isLittle ? .defaultHigh : .defaultLow
            bottomBigConstraint.priority = isLittle ? .defaultLow : .defaultHigh
            containerView.updateConstraints()
        }
    }
    
    //MARK:- Configure functions
    func configure(for event: Event)
    {
        imageEvent.layer.cornerRadius = 39
        containerView.layer.cornerRadius = 39
        nameLabel.text = event.nameEvent
        descriptionLabel.text = event.descriptionEvent
        if let image = event.image {
            imageEvent.image = UIImage(data: image)
        }
        else
        {
            imageEvent.image = #imageLiteral(resourceName: "pictureImage")
        }
        updateDateLabel(date: event.dateEvent!)
        updateHourLabel(date: event.dateEvent!)
        if let _ = event.locationEvent {
            locationLabel.text = event.locationEvent
        } else {
            locationLabel.text = "No address"
        }
        locationLabel.sizeToFit()
        
    }

    func updateDateLabel(date: Date) {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM, dd, yyyy"
        dateLabel.text = formatter.string(from: date)
    }
    func updateHourLabel(date: Date) {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        hourLabel.text = formatter.string(from: date)
    }
}
