//
//  PickLocationViewController.swift
//  Remainder App
//
//  Created by Andreea Sinescu on 23/07/2018.
//  Copyright © 2018 Andreea Sinescu. All rights reserved.
//

import UIKit
import BXProgressHUD
import MapKit

protocol PickLocationViewControllerDelegate: class {
    func pickLocationViewController(_ controller: PickLocationViewController, didFinishAdding placemark: CLPlacemark?, latitude: Double?, longitude: Double?)
}

class PickLocationViewController: UIViewController {
    
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerViewMap: UIView!
    @IBOutlet weak var buttonSearch: UIButton!
    
    var address: String?
    var oldAnnotation: MKPointAnnotation?
    var placemark: CLPlacemark?
    var latitude: Double?
    var longitude: Double?
    
    weak var delegate: PickLocationViewControllerDelegate?
    
    static func createView(delegate: PickLocationViewControllerDelegate, location: String?) -> PickLocationViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "PickLocationViewController") as! PickLocationViewController
        controller.delegate = delegate
        controller.address = location
        return controller
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.layer.cornerRadius = 28
        mapView.layer.masksToBounds = true
        mapView.clipsToBounds = true
        containerView.layer.cornerRadius = 28
        containerViewMap.layer.cornerRadius = 28
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        topConstraint.constant = -self.mapView.frame.height
        mapView.alpha = 0
        locationTextField.alpha = 0
        buttonSearch.alpha = 0
        saveButton.alpha = 0
        backButton.alpha = 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.8) {
            self.topConstraint.constant = 0
            self.mapView.alpha = 1
            self.locationTextField.alpha = 1
            self.buttonSearch.alpha = 1
            self.saveButton.alpha = 1
            self.backButton.alpha = 1
            self.view.layoutIfNeeded()
        }
        if let _ = address {
            generateWith(address: address!)
        }
    }

    //MARK:- Actions
    @IBAction func cancel(_ sender: Any) {
        locationTextField.resignFirstResponder()
        closeAnimation(placemark: nil)
    }
    
    @IBAction func done(_ sender: Any) {
        locationTextField.resignFirstResponder()
        if let _ = placemark {
            closeAnimation(placemark: placemark)
        } else {
            self.showAlert(title: "Whoops...", message: "Choose a location.")
        }
    }
    
    
    @IBAction func searchLocation(_ sender: Any) {
        locationTextField.resignFirstResponder()
        if let address = locationTextField.text {
            generateWith(address: address)
        } else {
            self.showAlert(title: "Whoops...", message: "Write a location.")
        }
    }
    
    //MARK:- Map functions
    func centerMap(coordinates: CLLocationCoordinate2D)
    {
        let region = MKCoordinateRegionMakeWithDistance(coordinates, 100000, 100000)
        mapView.setRegion(region, animated: true)
    }
    
    func generateWith(address: String)
    {
        let geocoder = CLGeocoder()
        _ = BXProgressHUD.showHUDAddedTo(self.view)
        geocoder.geocodeAddressString(address) { (placemarksOptional, error) -> Void in
            _ = BXProgressHUD.hideAllHUDsForView(self.view, animated: false)
            if let placemarks = placemarksOptional, let location = placemarks.first?.location  {
                if let _ = self.oldAnnotation {
                    self.mapView.removeAnnotation(self.oldAnnotation!)
                }
                let annotation = MKPointAnnotation()
                annotation.coordinate = location.coordinate
                self.mapView.addAnnotation(annotation)
                self.oldAnnotation = annotation
                self.placemark = placemarks.first!
                self.latitude = annotation.coordinate.latitude
                self.longitude = annotation.coordinate.longitude
                self.centerMap(coordinates: location.coordinate)
            } else {
                if let _ = self.oldAnnotation {
                    self.mapView.removeAnnotation(self.oldAnnotation!)
                }
                self.placemark = placemarksOptional?.first
                self.showAlert(title: "Whoops", message: "Location not found")
            }
        }
    }
    
    //MARK:- Animations
    func closeAnimation(placemark: CLPlacemark?) {
        UIView.animate(withDuration: 0.8, animations: {
            self.backButton.alpha = 0
            self.saveButton.alpha = 0
            self.topConstraint.constant = -self.mapView.frame.height
            self.mapView.alpha = 0
            self.containerView.alpha = 0
            self.view.layoutIfNeeded()
        }) { _ in
            if let _ = placemark {
                self.delegate?.pickLocationViewController(self, didFinishAdding: placemark, latitude: self.latitude, longitude: self.longitude)
                self.dismiss(animated: false, completion: nil)
            } else {
                self.dismiss(animated: false, completion: nil)
            }
        }
    }

}

