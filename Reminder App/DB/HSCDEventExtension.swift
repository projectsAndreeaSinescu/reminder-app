//
//  HSCDEventExtension.swift
//  Remainder App
//
//  Created by Andreea Sinescu on 19/07/2018.
//  Copyright © 2018 Andreea Sinescu. All rights reserved.
//

import Foundation

import Foundation
import CoreData

//MARK: Web convertions

extension Event
{
    /// Set data from web dictionary
    ///
    /// - Parameter dict: data
    func loadFromDict(_ dict : [String: Any])
    {
        self.nameEvent = dict["name"] as? String
        self.descriptionEvent = dict["description"] as? String
        self.dateEvent = Date(timeIntervalSince1970: dict["reminderAt"] as! TimeInterval)
        self.locationEvent = dict["location"] as? String
        self.latitude = dict["latitude"] as! Double
        self.longitude = dict["longitude"] as! Double
        self.minBefore = dict["alarm"] as! Int16
    }
    
    /// Build a dictionary based on the data - will be used to create json
    ///
    /// - Returns: the dictionary
    func getDict() -> [String: Any]
    {
        
        return [:]
    }
}


//MARK: CoreData extensions
extension HSCoreDataController
{
    
    func retrieveOrCreateEvent(_ uid : String) -> Event
    {
        let event = self.retriveOrCreateObject(forEntity: "Event", withField: "uid", matching: uid) as! Event
        
        return event
    }
    
    func createOrReplaceEvent(date: Date?, description: String?, image: Data?, location: String?, minBefore: Int, name: String?, uid: String) -> Event
    {
        let event = self.retriveOrCreateObject(forEntity: "Event",
                                              withField: "uid",
                                              matching: uid) as! Event
        event.dateEvent = date
        event.descriptionEvent = description
        event.image = image
        event.locationEvent = location
        event.minBefore = Int16(minBefore)
        event.nameEvent = name
    
        return event
    }
    
    func createOrReplaceEvent(_ dict : [String : Any]) -> Event
    {
        let event = self.retriveOrCreateObject(forEntity: "Event",
                                               withField: "uid",
                                               matching: "\(HSUtils.getNumber(obj: dict["id"] as AnyObject))") as! Event
        event.loadFromDict(dict)
        
        return event
    }
    
    func retrieveEvents() -> [Event]
    {
        let request = NSFetchRequest<Event>()
        let entity = NSEntityDescription.entity(forEntityName: "Event", in: self.managedObjectContext)
        request.entity = entity
        
        do
        {
            let result = try self.managedObjectContext.fetch(request)
            return result
        }
        catch
        {
            HSLogManager.staticInstance.writeToLogs("Error in \(#function) \(error)")
        }
        
        return [Event]()
    }
    
    
}
