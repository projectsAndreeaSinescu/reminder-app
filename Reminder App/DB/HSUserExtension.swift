//
//  HSUserExtension.swift
//  Remainder App
//
//  Created by Andreea Sinescu on 10/08/2018.
//  Copyright © 2018 Andreea Sinescu. All rights reserved.
//

//
//  UserExtension.swift
//  Login
//
//  Created by Andreea Sinescu on 07/08/2018.
//  Copyright © 2018 Andreea Sinescu. All rights reserved.
//

import Foundation
import CoreData

extension User
{
    /// Set data from web dictionary
    ///
    /// - Parameter dict: data
    func loadFromDict(_ dict : [String: Any])
    {
        self.email = dict["email"] as? String
        self.username = dict["username"] as? String
    }
    
    /// Build a dictionary based on the data - will be used to create json
    ///
    /// - Returns: the dictionary
    func getDict() -> [String: Any] {
        let dict = ["email":self.email!,
                    "username": self.username!]
        return dict
    }
}

//MARK: CoreData extensions
extension HSCoreDataController
{
    
    func retrieveOrCreateUser(_ uid : NSNumber) -> User
    {
        let room = self.retriveOrCreateObject(forEntity: "User", withField: "uid", matching: uid) as! User
        
        return room
    }
    
    func createOrReplaceUsers(_ dicts : [[String : Any]]) -> [User]
    {
        var users = [User]()
        for dict in dicts
        {
            let user = self.retriveOrCreateObject(forEntity: "User",
                                                  withField: "uid",
                                                  matching: HSUtils.getNumber(obj: dict["id"] as AnyObject)) as! User
            user.loadFromDict(dict)
            users.append(user)
        }
        
        return users
    }
    
    
    func createOrReplaceUser(_ dict : [String : Any]) -> User
    {
        let user = self.retriveOrCreateObject(forEntity: "User",
                                              withField: "uid",
                                              matching: HSUtils.getNumber(obj: dict["id"] as AnyObject)) as! User
        user.loadFromDict(dict)
        
        return user
    }
    
    
    func retrieveUsers() -> [User]
    {
        let request = NSFetchRequest<User>()
        let entity = NSEntityDescription.entity(forEntityName: "User", in: self.managedObjectContext)
        request.entity = entity
        
        do
        {
            let result = try self.managedObjectContext.fetch(request)
            return result
        }
        catch
        {
            HSLogManager.staticInstance.writeToLogs("Error in \(#function) \(error)")
        }
        
        return [User]()
    }
    
    
}

