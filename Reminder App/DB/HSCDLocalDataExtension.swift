//
//  HSCDLocalDataExtension.swift
//  CrossLife
//
//  Created by Lucian Aghergheloaei on 26/01/17.
//  Copyright © 2017 Lazar Mirela. All rights reserved.
//

import UIKit

//TODO change based on project
extension HSCoreDataController {
    
    private func loadLocalData(forResource: String, withExtension: String) -> [[String: Any]] {
        guard let localData = Bundle.main.url(forResource: forResource, withExtension: withExtension) else {
            HSLogManager.staticInstance.writeToLogs("\(#function) \(forResource): No local data found")
            return []
        }
        do {
            let jsonData = try Data(contentsOf: localData)
            guard let data = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [[String: Any]] else {
                HSLogManager.staticInstance.writeToLogs("\(#function) \(forResource): Could not parse series")
                return []
            }
            
            return data
        } catch {
            HSLogManager.staticInstance.writeToLogs("\(#function) \(forResource) : \(error)")
            return []
        }
    }
}
