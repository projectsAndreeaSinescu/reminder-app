//
//  WebService.swift
//  Remainder App
//
//  Created by Andreea Sinescu on 10/08/2018.
//  Copyright © 2018 Andreea Sinescu. All rights reserved.
//

import UIKit
import AFNetworking

public class WebService: AFHTTPSessionManager {
    public typealias CompletionBlock = (_ completion: HSWebServiceResponse) -> Void
    public typealias ProgressBlock = (_: Float) -> Void
    
    public static var sharedInstance = WebService()
    
    static var myBaseURL: String {
        get {
            return "http://192.168.65.101:3100"
        }
    }
    
    static var apiURI: String = "/api/v1"
    var extensionsData = [String: Any]()
    
    public init() {
        let url = NSURL(string: "\(WebService.myBaseURL)\(WebService.apiURI)") as URL?
        super.init(baseURL: url, sessionConfiguration: nil)
        requestSerializer = AFJSONRequestSerializer()
        
        self.securityPolicy = AFSecurityPolicy(pinningMode: .none)
        self.securityPolicy.allowInvalidCertificates = true
        self.securityPolicy.validatesDomainName = false
        
        self.reachabilityManager.startMonitoring()
        self.reachabilityManager.setReachabilityStatusChange { (status) in
            self.reachabilityUpdatedStatus(status)
        }
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- Reachability
    var isReachableOrUndetermined: Bool {
        if self.reachabilityManager.isReachable {
            return true
        } else if (self.reachabilityManager.networkReachabilityStatus == .unknown) {
            return true
        } else {
            return false
        }
    }
    
    func reachabilityUpdatedStatus(_ state: AFNetworkReachabilityStatus) {
        if self.reachabilityManager.isReachable {
            restartSync()
        }
    }
    
    func restartSync() {
        guard self.currentUser != nil else {
            return
        }
    }
    
    //MARK:- Authentification
    var accessToken: String?
    var accessTokenExpiration: Date?
    var currentUser: User?
    var url: String?
    
    var hasValidAccessToken: Bool {
        if accessToken == nil || accessTokenExpiration == nil {
            return false
        } else {
            return accessTokenExpiration!.timeIntervalSinceNow > 0
        }
    }
    
    func checkAccessTokenAndContinue(completion: @escaping CompletionBlock)
    {
        guard reachabilityManager.isReachable else
        {
            let response = HSWebServiceResponse(code: HSWebServiceResponseCode.NoConnectivity)
            completion(response)
            return
        }
        
        guard currentUser != nil else
        {
            let response = HSWebServiceResponse(code: HSWebServiceResponseCode.NoUser)
            completion(response)
            return
        }
        if self.hasValidAccessToken
        {
            let response = HSWebServiceResponse(code: HSWebServiceResponseCode.Success)
            response.parsedResult = self.accessToken
            completion(response)
            return
        }
    }
    
    private var _passwordText: String?
    var settingsLoaded = false
    var minPassword: Int = 1
    var maxPassword: Int = 25
    var passwordText: String {
        if _passwordText != nil {
            return _passwordText!
        }
        if maxPassword > 0 {
            return "Password must have between \(minPassword) and \(maxPassword) characters"
        }
        return "Password must have at least \(minPassword) characters"
    }
    
    public func apiSettings(completion: @escaping CompletionBlock) {
        guard !settingsLoaded else {
            let response = HSWebServiceResponse(code: HSWebServiceResponseCode.Success)
            completion(response)
            return
        }
        
        guard self.isReachableOrUndetermined else {
            let response = HSWebServiceResponse(code: HSWebServiceResponseCode.NoConnectivity)
            completion(response)
            return
        }
        
        let generalErrorResponse = "Failed to connect to server"
        let address = "clientusers/apiSettings"
        
        self.get(address, parameters: nil, progress: nil, success: { (task, result) in
            guard let resultDict = result as? [String : Any] else {
                let response = HSWebServiceResponse(code: HSWebServiceResponseCode.ResponseStructureError)
                completion(response)
                return
            }
            
            guard let responseDict = resultDict["response"] as? [String : Any] else {
                HSLogManager.staticInstance.writeToLogs("apiSettings result: \(resultDict)")
                let response = HSWebServiceResponse(code: HSWebServiceResponseCode.ResponseStructureError)
                response.errorMessage = generalErrorResponse
                completion(response)
                return
            }
            
            if let fileStorage = responseDict["fileStorage"] as? [String : Any] {
                if let url = fileStorage["url"] as? String {
                    self.url = url
                }
            }
            
            if let passwordRules = responseDict["passwordRules"] as? [String : Any] {
                if let text = passwordRules["text"] as? String {
                    self._passwordText = text
                }
                if let min = passwordRules["min"] as? Int {
                    self.minPassword = min
                }
                if let max = passwordRules["max"] as? Int {
                    self.maxPassword = max
                }
                self.settingsLoaded = true
                let response = HSWebServiceResponse(code: HSWebServiceResponseCode.Success)
                completion(response)
            }
        }) { (task, error) in
            debugPrint(error)
            let response = HSWebServiceResponse(urlResponse: task?.response)
            response.errorMessage = generalErrorResponse
            completion(response)
        }
    }
    
    public func register(email: String, password: String, username: String, completion: @escaping CompletionBlock) {
        guard self.isReachableOrUndetermined else {
            let response = HSWebServiceResponse(code: HSWebServiceResponseCode.NoConnectivity)
            completion(response)
            return
        }
        
        let params = ["email" : email, "password": password, "username" : username]
        let generalErrorResponse = "Failed to register"
        let address = "clientusers/register"
        
        self.post(address, parameters: params, progress: nil, success: { (task, result) in
            guard let resultDict = result as? [String : Any] else {
                let response = HSWebServiceResponse(code: HSWebServiceResponseCode.ResponseStructureError)
                completion(response)
                return
            }
            let response = HSWebServiceResponse(resultDict)
            guard response.isSuccess else {
                HSLogManager.staticInstance.writeToLogs("\(generalErrorResponse) result: \(resultDict)")
                completion(response)
                return
            }
            response.parsedResult = email
            completion(response)
        }) { (task, error) in
            debugPrint(error)
            let response = HSWebServiceResponse(urlResponse: task?.response)
            response.errorMessage = generalErrorResponse
            completion(response)
        }
    }
    
    public func login(email: String, password: String, completion: @escaping CompletionBlock) {
        guard self.isReachableOrUndetermined else {
            let response = HSWebServiceResponse(code: HSWebServiceResponseCode.NoConnectivity)
            completion(response)
            return
        }
        
        let params = ["email" : email, "password": password]
        let generalErrorResponse = "Failed to login"
        let address = "clientusers/clientLogin"
        
        self.post(address, parameters: params, progress: nil, success: { (task, result) in
            guard let resultDict = result as? [String : Any] else {
                let response = HSWebServiceResponse(code: HSWebServiceResponseCode.ResponseStructureError)
                completion(response)
                return
            }
            
            let response = self.parseLoginResponse(resultDict)
            guard response.isSuccess else {
                HSLogManager.staticInstance.writeToLogs("Login result: \(resultDict)")
                completion(response)
                return
            }
            completion(response)
            
        }) { (task, error) in
            debugPrint(error)
            let response = HSWebServiceResponse(urlResponse: task?.response)
            response.errorMessage = generalErrorResponse
            completion(response)
        }
        
    }
    
    public func logout(completion: @escaping CompletionBlock) {
        guard self.isReachableOrUndetermined else {
            let response = HSWebServiceResponse(code: HSWebServiceResponseCode.NoConnectivity)
            completion(response)
            return
        }
        
        guard self.accessToken != nil else {
            self.cleanUserData()
            let response = HSWebServiceResponse(code: HSWebServiceResponseCode.Success)
            completion(response)
            return
        }
        
        let generalErrorResponse = "Failed to log out"
        let address = "clientusers/clientLogout?access_token=\(self.accessToken!)"
        let params = ["email":currentUser!.email]
        
        self.post(address, parameters: params, progress: nil, success: { (task, result) in
            self.cleanUserData()
            
            guard let resultDict = result as? [String : Any] else {
                let response = HSWebServiceResponse(code: HSWebServiceResponseCode.ResponseStructureError)
                response.errorMessage = generalErrorResponse
                completion(response)
                return
            }
            
            let response = HSWebServiceResponse(resultDict)
            guard response.isSuccess else {
                HSLogManager.staticInstance.writeToLogs("error: \(generalErrorResponse), for result: \(resultDict)")
                completion(response)
                return
            }
            
            completion(response)
        }) { (task, error) in
            debugPrint(error)
            let response = HSWebServiceResponse(urlResponse: task?.response)
            response.errorMessage = generalErrorResponse
            completion(response)
        }
    }
    
    func parseLoginResponse(_ resultDict: [String : Any]) -> HSWebServiceResponse {
        let generalErrorResponse = "Login failed"
        let response = HSWebServiceResponse(resultDict)
        guard response.isSuccess else {
            return response
        }
        
        guard let responseDict = resultDict["response"] as? [String : Any] else {
            HSLogManager.staticInstance.writeToLogs("login result: \(resultDict)")
            let response = HSWebServiceResponse(code: HSWebServiceResponseCode.ResponseStructureError)
            response.errorMessage = generalErrorResponse
            return response
        }
        
        guard let accessToken = responseDict["id"] as? String else {
            HSLogManager.staticInstance.writeToLogs("login result: \(resultDict)")
            let response = HSWebServiceResponse(code: HSWebServiceResponseCode.ResponseStructureError)
            response.errorMessage = generalErrorResponse
            return response
        }
        
        guard let userDict = responseDict["user"] as? [String : Any] else {
            HSLogManager.staticInstance.writeToLogs("login result: \(resultDict)")
            let response = HSWebServiceResponse(code: HSWebServiceResponseCode.ResponseStructureError)
            response.errorMessage = generalErrorResponse
            return response
            
        }
        
        guard let tokenCreatedString = responseDict["created"] as? String else {
            HSLogManager.staticInstance.writeToLogs("Token Created string is missing: \(responseDict)")
            let response = HSWebServiceResponse(code: HSWebServiceResponseCode.ResponseStructureError)
            response.errorMessage = generalErrorResponse
            return response
            
        }
        
        guard responseDict["ttl"] != nil else {
            HSLogManager.staticInstance.writeToLogs("Token TTL string is missing: \(responseDict)")
            let response = HSWebServiceResponse(code: HSWebServiceResponseCode.ResponseStructureError)
            response.errorMessage = generalErrorResponse
            return response
            
        }
        
        let ttl = HSUtils.getDouble(obj: responseDict["ttl"] as AnyObject)
        
        guard let tokenCreatedDate = HSDateFormatter.getDate(dateString: tokenCreatedString,
                                                             dateFormat: HSDateFormatter.KnownFormat.ISODateTime.rawValue,
                                                             timeZoneFormat: HSDateFormatter.KnownTimezones.UTC.rawValue) else {
                                                                
                                                                HSLogManager.staticInstance.writeToLogs("Token Created date string is missing: \(responseDict)")
                                                                let response = HSWebServiceResponse(code: HSWebServiceResponseCode.ResponseStructureError)
                                                                response.errorMessage = generalErrorResponse
                                                                return response
        }
        
        
        self.accessTokenExpiration = tokenCreatedDate.addingTimeInterval(ttl)
        self.accessToken = accessToken
        
        let user = HSCoreDataController.sharedInstance.createOrReplaceUser(userDict)
        self.currentUser = user
        self.restartSync()
        
        response.parsedResult = user
        return response
    }
    
    func cleanUserData() {
        self.accessToken = nil
    }
    
}

extension WebService {
    public func saveReminder(event: Event, completion: @escaping CompletionBlock) {
        guard self.isReachableOrUndetermined else {
            let response = HSWebServiceResponse(code: HSWebServiceResponseCode.NoConnectivity)
            completion(response)
            return
        }
        
        self.checkAccessTokenAndContinue { (result) in
            guard result.isSuccess else {
                completion(result)
                return
            }
            var params = ["userID": self.currentUser!.uid, "name": event.nameEvent!, "description": event.descriptionEvent!, "alarm": event.minBefore, "reminderAt": event.dateEvent!.timeIntervalSince1970] as [String : Any]
            if let _ = event.locationEvent {
                params["location"] = event.locationEvent!
                params["latitude"] = event.latitude
                params["longitude"] = event.longitude
            }
            let generalErrorResponse = "Failed to register"
            let address = "reminders/saveReminder?access_token=\(self.accessToken!)"
            
            self.post(address, parameters: params, progress: nil, success: { (task, result) in
                guard let resultDict = result as? [String : Any] else {
                    let response = HSWebServiceResponse(code: HSWebServiceResponseCode.ResponseStructureError)
                    completion(response)
                    return
                }
                let response = HSWebServiceResponse(resultDict)
                guard response.isSuccess else {
                    HSLogManager.staticInstance.writeToLogs("\(generalErrorResponse) result: \(resultDict)")
                    completion(response)
                    return
                }
                response.parsedResult = event
                completion(response)
            }) { (task, error) in
                debugPrint(error)
                let response = HSWebServiceResponse(urlResponse: task?.response)
                response.errorMessage = generalErrorResponse
                completion(response)
            }
        }
    }
    
    public func getReminders(completion: @escaping CompletionBlock) {
        guard self.isReachableOrUndetermined else {
            let response = HSWebServiceResponse(code: HSWebServiceResponseCode.NoConnectivity)
            completion(response)
            return
        }
        
        self.checkAccessTokenAndContinue { (result) in
            guard result.isSuccess else {
                completion(result)
                return
            }
            let generalErrorResponse = "Failed to get reminders"
            let address = "reminders/getReminders?access_token=\(self.accessToken!)"
            let params = ["limit": Double(0), "page": Double(0)]
            
            self.get(address, parameters: params, progress: nil, success: { (task, result) in
                guard let resultDict = result as? [String : Any] else {
                    let response = HSWebServiceResponse(code: HSWebServiceResponseCode.ResponseStructureError)
                    completion(response)
                    return
                }
                
                let response = self.parseGetRemindersResponse(resultDict)
                
                guard response.isSuccess else {
                    HSLogManager.staticInstance.writeToLogs("Get reminders result: \(resultDict)")
                    completion(response)
                    return
                }
                
                completion(response)
            }, failure: { (task, error) in
                debugPrint(error)
                let response = HSWebServiceResponse(urlResponse: task?.response)
                response.errorMessage = generalErrorResponse
                completion(response)
            })
        }
    }
    
    func parseGetRemindersResponse(_ resultDict: [String : Any]) -> HSWebServiceResponse {
        let generalErrorResponse = "Failed to get reminders"
        let response = HSWebServiceResponse(resultDict)
        guard response.isSuccess else {
            return response
        }
        
        guard let responseDict = resultDict["response"] as? NSArray else {
            HSLogManager.staticInstance.writeToLogs("login result: \(resultDict)")
            let response = HSWebServiceResponse(code: HSWebServiceResponseCode.ResponseStructureError)
            response.errorMessage = generalErrorResponse
            return response
        }
        
        var events:[Event] = []
        
        for event in responseDict {
            let dict = event as? [String : Any]
            print(dict)
            let newEvent = HSCoreDataController.sharedInstance.createOrReplaceEvent(dict!)
            events.append(newEvent)
        }
        print(events)
        response.parsedResult = events
        return response
    }
}
