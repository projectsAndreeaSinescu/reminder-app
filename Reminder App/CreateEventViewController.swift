//
//  CreateEventViewController.swift
//  Remainder App
//
//  Created by Andreea Sinescu on 19/07/2018.
//  Copyright © 2018 Andreea Sinescu. All rights reserved.
//

import UIKit
import MapKit
import IQKeyboardManager
import UserNotifications

protocol CreateEventViewControllerDelegate: class {
    func createEventViewController(_ controller: CreateEventViewController, didFinishAdding event: Event)
    func createEventViewController(_ controller: CreateEventViewController, didFinishEditing event: Event)
    func createEventViewControllerCancel(_ controller: CreateEventViewController)
}

class CreateEventViewController: UIViewController {
    
    @IBOutlet weak var createEventView: UIView!
    @IBOutlet weak var addImageButton: UIButton!
    @IBOutlet weak var addLocationButton: UIButton!
    @IBOutlet weak var fiveMinButton: UIButton!
    @IBOutlet weak var tenMinButton: UIButton!
    @IBOutlet weak var fifteenMinButton: UIButton!
    @IBOutlet weak var finishButton: UIButton!
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var createEventLabel: UILabel!
    @IBOutlet weak var animatedImageView: UIImageView!
    @IBOutlet weak var charactersName: UILabel!
    @IBOutlet weak var charactersDescription: UILabel!
    @IBOutlet weak var pickDate: UITextView!
    @IBOutlet weak var pickTime: UITextView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var alarmLabel: UILabel!
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var viewDescription: UIView!
    @IBOutlet weak var fiveImageView: UIImageView!
    @IBOutlet weak var tenImageView: UIImageView!
    @IBOutlet weak var fifteenImageView: UIImageView!
    @IBOutlet weak var fiveImageSelectedView: UIImageView!
    @IBOutlet weak var tenImageSelectedView: UIImageView!
    @IBOutlet weak var fifteenSelectedImageView: UIImageView!
    @IBOutlet weak var fiveLabel: UILabel!
    @IBOutlet weak var tenLabel: UILabel!
    @IBOutlet weak var fifteenLabel: UILabel!
    @IBOutlet weak var widthFiveConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightFiveConstraint: NSLayoutConstraint!
    @IBOutlet weak var widthTenConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightTenConstraint: NSLayoutConstraint!
    @IBOutlet weak var widthFifteenConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightFifteenConstraint: NSLayoutConstraint!
    @IBOutlet weak var topFiveButtonConstraint: NSLayoutConstraint!
    @IBOutlet weak var topTenButtonConstraint: NSLayoutConstraint!
    @IBOutlet weak var topFifteenButtonConstraint: NSLayoutConstraint!
    @IBOutlet weak var leftButtonsConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightButtonsConstraint: NSLayoutConstraint!
    @IBOutlet weak var pickLocationView: UIView!
    @IBOutlet weak var imageButtonView: UIImageView!
    
    weak var delegate: CreateEventViewControllerDelegate?
    var image: UIImage?
    var eventToEdit: Event?
    var address: String?
    var latitude: Double?
    var longitude: Double?
    var initialDate: Date?
    var date = Date()
    var datePicker: UIDatePicker!
    var timePicker: UIDatePicker!
    var minBefore = 0
    var height: CGFloat?
    var first = 0
    var backFromPickLocation = false
    var backFromPickImage = false
    
    static func createViewController(delegate: CreateEventViewControllerDelegate, for eventToEdit: Event?) -> CreateEventViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "CreateEventViewController") as! CreateEventViewController
        controller.delegate = delegate
        controller.eventToEdit = eventToEdit
        return controller
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        finishButton.setImage(#imageLiteral(resourceName: "okButton"), for: .normal)
        finishButton.setBackgroundImage(nil, for: .normal)
        finishButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        timePicker = UIDatePicker()
        timePicker.datePickerMode = .time
        
        pickLocationView.layer.cornerRadius = 28
        
        createEventView.layer.cornerRadius = 28
        nameTextField.addTarget(self, action: #selector(nameDidChange), for: .editingChanged)
        datePicker.addTarget(self, action: #selector(dateChanged), for: .valueChanged)
        timePicker.addTarget(self, action: #selector(timeChanged), for: .valueChanged)
        var gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(close))
        gestureRecognizer.cancelsTouchesInView = false
        gestureRecognizer.delegate = self
        addImageButton.layer.cornerRadius = 39
        imageButtonView.layer.cornerRadius = 39
        
        fiveMinButton.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).cgColor
        fiveMinButton.layer.cornerRadius = fiveMinButton.frame.size.width / 2
        fiveMinButton.layer.borderWidth = 1
        tenMinButton.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).cgColor
        tenMinButton.layer.cornerRadius = tenMinButton.frame.size.width / 2
        tenMinButton.layer.borderWidth = 1
        fifteenMinButton.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).cgColor
        fifteenMinButton.layer.cornerRadius = fifteenMinButton.frame.size.width / 2
        fifteenMinButton.layer.borderWidth = 1
        
        if let event = eventToEdit {
            minBefore = Int(eventToEdit!.minBefore)
            createEventLabel.text = "Edit Event"
            nameTextField.text = event.nameEvent
            descriptionTextView.text = event.descriptionEvent
            if let image = event.image {
                imageButtonView.image = UIImage(data: image)
            }
            if event.minBefore == 5 {
                fiveMinButton.isSelected = true
            } else if event.minBefore == 10 {
                tenMinButton.isSelected = true
            } else if event.minBefore == 15 {
                fifteenMinButton.isSelected = true
            }
            if date < event.dateEvent! {
                self.showAlert(title: "Whoops...", message: "You can't choose this date anymore. Choose another one!")
            }
            datePicker.minimumDate = date
            timePicker.minimumDate = date
            initialDate = date
            date = event.dateEvent!
            updateDateLabel()
            updateHourLabel()
            if let _ = eventToEdit?.locationEvent {
                address = eventToEdit!.locationEvent
                addLocationButton.setBackgroundImage(#imageLiteral(resourceName: "locationDone"), for: .normal)
            }
            datePicker.date = event.dateEvent!
            timePicker.date = event.dateEvent!
        } else {
            date = date.adding(minutes: 120)
            initialDate = date
            datePicker.minimumDate = date
            timePicker.minimumDate = date
            updateDateLabel()
            updateHourLabel()
        }
        pickDate.inputView = datePicker
        pickTime.inputView = timePicker
        if let _ = nameTextField.text?.count {
            charactersName.text = "\(17-nameTextField.text!.count)/17 characters"
        }
        if let _ = descriptionTextView.text?.count {
            let existingLines = descriptionTextView.text!.components(separatedBy: CharacterSet.newlines)
            let number = (existingLines.count - 1) * 37 + existingLines.last!.count
            
            charactersDescription.text = "\(120 - number)/120 characters"
        }
        view.addGestureRecognizer(gestureRecognizer)
        gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        createEventView.addGestureRecognizer(gestureRecognizer)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if backFromPickLocation == true {
            backFromPickLocation = false
            UIView.animate(withDuration: 0.6) {
                self.createEventView.alpha = 1
                self.filterButton.alpha = 1
            }
        } else {
            createEventAnimation()
            print(tenImageSelectedView.alpha)
            if first == 0 {
                first = 1
                openCreateEventAnimation()
                buttonsAnimation()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if backFromPickLocation == false {
            self.dateLabel.alpha = 0
            self.hourLabel.alpha = 0
            self.nameTextField.alpha = 0
            self.descriptionTextView.alpha = 0
            self.charactersName.alpha = 0
            self.charactersDescription.alpha = 0
            self.addImageButton.alpha = 0
            self.imageButtonView.alpha = 0
            self.addLocationButton.alpha = 0
            self.fiveMinButton.alpha = 0
            self.tenMinButton.alpha = 0
            self.fifteenMinButton.alpha = 0
            self.pickDate.alpha = 0
            self.pickTime.alpha = 0
            self.nameLabel.alpha = 0
            self.descriptionLabel.alpha = 0
            self.alarmLabel.alpha = 0
            self.viewDescription.alpha = 0
            self.viewName.alpha = 0
            self.fiveImageView.alpha = 0
            self.fiveLabel.alpha = 0
            self.tenImageView.alpha = 0
            self.tenLabel.alpha = 0
            self.fifteenImageView.alpha = 0
            self.fifteenLabel.alpha = 0
            self.fiveImageSelectedView.alpha = 0
            self.tenImageSelectedView.alpha = 0
            self.fifteenSelectedImageView.alpha = 0
        } else {
            createEventView.alpha = 0
            filterButton.alpha = 0
        }
    }

    //MARK:- Gesture Recognizer functions
    @objc func close() {
        nameTextField.resignFirstResponder()
        descriptionTextView.resignFirstResponder()
        pickDate.resignFirstResponder()
        pickTime.resignFirstResponder()
        withoutContaint(event: nil)
    }
    
    @objc func hideKeyboard() {
        nameTextField.resignFirstResponder()
        descriptionTextView.resignFirstResponder()
        pickDate.resignFirstResponder()
        pickTime.resignFirstResponder()
        var components = Calendar.current.dateComponents([.month, .day, .year, .hour, .minute], from: datePicker.date)
        let time = Calendar.current.dateComponents([.hour, .minute], from: timePicker.date)
        components.hour = time.hour
        components.minute = time.minute
        date = Calendar.current.date(from: components)!
        updateHourLabel()
        updateDateLabel()
    }
    
    @objc func dateChanged()
    {
        var components = Calendar.current.dateComponents([.month, .day, .year, .hour, .minute], from: datePicker.date)
        let time = Calendar.current.dateComponents([.hour, .minute], from: timePicker.date)
        components.hour = time.hour
        components.minute = time.minute
        date = Calendar.current.date(from: components)!
        updateHourLabel()
        updateDateLabel()
    }
    
    @objc func timeChanged()
    {
        var components = Calendar.current.dateComponents([.month, .day, .year, .hour, .minute], from: datePicker.date)
        let time = Calendar.current.dateComponents([.hour, .minute], from: timePicker.date)
        components.hour = time.hour
        components.minute = time.minute
        date = Calendar.current.date(from: components)!
        updateHourLabel()
        updateDateLabel()
    }
    
    @objc func nameDidChange()
    {
        if let text = nameTextField.text
        {
            if (text.count > 17)
            {
                let start = text.startIndex
                nameTextField.text = text.substring(to: text.index(start, offsetBy: 17))
            }
            if text.count <= 17 {
                charactersName.text = "\(17-text.count)/17 characters"
            } else {
                charactersName.text = "0/17 characters"
            }
        }
    }
    
    //MARK:- Update date and time functions
    func updateDateLabel() {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM, dd, yyyy"
        dateLabel.text = formatter.string(from: date)
    }
    
    func updateHourLabel() {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        hourLabel.text = formatter.string(from: date)
    }
    
    //MARK:- Location functions
    func addLocation() {
        animatedImageView.frame.size = addLocationButton.frame.size
        var origin = addLocationButton.frame.origin
        origin.y += addLocationButton.superview!.frame.origin.y
        animatedImageView.frame.origin = origin
        addLocationButton.isHidden = true
        animatedImageView.alpha = 1
        
        backFromPickLocation = true
        UIView.animate(withDuration: 0.4,
                       animations: {
                        self.animatedImageView.frame = self.view.frame
                        self.animatedImageView.alpha = 0
                        self.createEventView.alpha = 0
                        self.pickLocationView.alpha = 1
        }) { (_) in
            let location = self.eventToEdit?.locationEvent
            let controller = PickLocationViewController.createView(delegate: self, location: location)
            self.present(controller, animated: false, completion: {
                self.addLocationButton.isHidden = false
                self.createEventView.alpha = 1
                self.pickLocationView.alpha = 0
            })
        }
    }
    
    //MARK:- Animations
    func openCreateEventAnimation() {
        if let _ = eventToEdit, let _ = eventToEdit!.locationEvent {
            animatedImageView.image = #imageLiteral(resourceName: "locationDone")
        } else {
            animatedImageView.image = #imageLiteral(resourceName: "addLocation")
        }
        animatedImageView.frame.size = CGSize(width: 1, height: 1)
        var origin = addLocationButton.frame.origin
        origin.y = origin.y + addLocationButton.superview!.frame.origin.y + addLocationButton.frame.size.height - 1
        origin.x = origin.x + addLocationButton.frame.size.width - 1
        animatedImageView.frame.origin = origin
        addLocationButton.isHidden = true
        animatedImageView.alpha = 1
        
        view.layoutIfNeeded()
        UIView.animate(withDuration: 0.6,
                       animations: {
                        self.animatedImageView.frame = self.addLocationButton.frame
                        self.animatedImageView.frame.origin.y += self.addLocationButton.superview!.frame.origin.y
                        self.view.layoutIfNeeded()
        }) { (_) in
            self.animatedImageView.alpha = 0
            self.addLocationButton.isHidden = false
        }
    }
    
    func buttonsAnimation() {
        widthFiveConstraint.constant = 0
        heightFiveConstraint.constant = 0
        topFiveButtonConstraint.constant += 41
        leftButtonsConstraint.constant += 42
        fiveMinButton.frame.origin.x = fiveMinButton.frame.origin.x - 41
        self.fiveMinButton.alpha = 1
        self.tenMinButton.alpha = 0
        self.fifteenMinButton.alpha = 0
        self.fiveImageView.alpha = 1
        self.fiveLabel.alpha = 1
        self.tenImageView.alpha = 0
        self.tenLabel.alpha = 0
        self.fifteenImageView.alpha = 0
        self.fifteenLabel.alpha = 0
        if fiveMinButton.isSelected == true {
            self.fiveImageSelectedView.alpha = 1
        }
        view.layoutIfNeeded()
        UIView.animate(withDuration: 0.2, animations: {
            self.widthFiveConstraint.constant = 42
            self.heightFiveConstraint.constant = 41
            self.topFiveButtonConstraint.constant -= 41
            self.leftButtonsConstraint.constant -= 42
            self.fiveMinButton.frame.origin.x = self.fiveMinButton.frame.origin.x + 41
            self.view.layoutIfNeeded()
        }) { (_) in
            self.tenMinButton.alpha = 1
            self.tenImageView.alpha = 1
            self.tenLabel.alpha = 1
            self.topTenButtonConstraint.constant += 41
            self.rightButtonsConstraint.constant += 42
            self.widthTenConstraint.constant = 0
            self.heightTenConstraint.constant = 0
            if self.tenMinButton.isSelected == true {
                self.tenImageSelectedView.alpha = 1
            }
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 0.2, animations: {
                self.rightButtonsConstraint.constant -= 42
                self.topTenButtonConstraint.constant -= 41
                self.widthTenConstraint.constant = 42
                self.heightTenConstraint.constant = 41
                self.view.layoutIfNeeded()
            }) {
                (_) in
                self.fifteenMinButton.alpha = 1
                self.fifteenImageView.alpha = 1
                self.fifteenLabel.alpha = 1
                self.topFifteenButtonConstraint.constant += 41
                self.widthFifteenConstraint.constant = 0
                self.heightFifteenConstraint.constant = 0
                if self.fifteenMinButton.isSelected == true {
                    self.fifteenSelectedImageView.alpha = 1
                }
                self.view.layoutIfNeeded()
                UIView.animate(withDuration: 0.2, animations: {
                    self.topFifteenButtonConstraint.constant -= 41
                    self.widthFifteenConstraint.constant = 42
                    self.heightFifteenConstraint.constant = 41
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    func createEventAnimation() {
        
        UIView.animate(withDuration: 0.6,
                       animations: {
                        self.dateLabel.alpha = 1
                        self.hourLabel.alpha = 1
                        self.nameTextField.alpha = 1
                        self.descriptionTextView.alpha = 1
                        self.charactersName.alpha = 1
                        self.charactersDescription.alpha = 1
                        self.addImageButton.alpha = 1
                        self.imageButtonView.alpha = 1
                        self.imageButtonView.alpha = 1
                        self.addLocationButton.alpha = 1
                        self.pickDate.alpha = 1
                        self.pickTime.alpha = 1
                        self.nameLabel.alpha = 1
                        self.descriptionLabel.alpha = 1
                        self.alarmLabel.alpha = 1
                        self.viewName.alpha = 1
                        self.viewDescription.alpha = 1
                        self.fiveMinButton.alpha = 1
                        self.tenMinButton.alpha = 1
                        self.fifteenMinButton.alpha = 1
                        self.fiveImageView.alpha = 1
                        self.fiveLabel.alpha = 1
                        self.tenImageView.alpha = 1
                        self.tenLabel.alpha = 1
                        self.fifteenImageView.alpha = 1
                        self.fifteenLabel.alpha = 1
                        if self.backFromPickImage == true
                        {
                            if self.fiveMinButton.isSelected {
                                self.fiveImageSelectedView.alpha = 1
                            } else if self.tenMinButton.isSelected {
                                self.tenImageSelectedView.alpha = 1
                            } else if self.fifteenMinButton.isSelected {
                                self.fifteenSelectedImageView.alpha = 1
                            }
                        }
        }) { _ in
            self.backFromPickImage = false
        }
    }
    
    func withoutContaint(event: Event?) {
        self.finishButton.setImage(#imageLiteral(resourceName: "plus"), for: .normal)
        self.finishButton.setBackgroundImage(#imageLiteral(resourceName: "Oval"), for: .normal)
        self.finishButton.imageEdgeInsets = UIEdgeInsets(top: 6, left: 0, bottom: 0, right: 0)
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.6,
                       animations: {
                        self.dateLabel.alpha = 0
                        self.hourLabel.alpha = 0
                        self.nameTextField.alpha = 0
                        self.descriptionTextView.alpha = 0
                        self.charactersName.alpha = 0
                        self.charactersDescription.alpha = 0
                        if self.eventToEdit != nil || event == nil{
                            self.imageButtonView.alpha = 0
                        }
                        self.addImageButton.alpha = 0
                        self.addLocationButton.alpha = 0
                        self.fiveMinButton.alpha = 0
                        self.tenMinButton.alpha = 0
                        self.fifteenMinButton.alpha = 0
                        self.pickDate.alpha = 0
                        self.pickTime.alpha = 0
                        self.nameLabel.alpha = 0
                        self.descriptionLabel.alpha = 0
                        self.alarmLabel.alpha = 0
                        self.viewDescription.alpha = 0
                        self.viewName.alpha = 0
                        self.fiveImageView.alpha = 0
                        self.fiveImageSelectedView.alpha = 0
                        self.tenImageSelectedView.alpha = 0
                        self.fifteenSelectedImageView.alpha = 0
                        self.fiveLabel.alpha = 0
                        self.tenImageView.alpha = 0
                        self.tenLabel.alpha = 0
                        self.fifteenImageView.alpha = 0
                        self.fifteenLabel.alpha = 0
        }) { _ in
            self.view.layoutIfNeeded()
            if let _ = event {
                if let _ = self.eventToEdit {
                    self.delegate?.createEventViewController(self, didFinishEditing: event!)
                } else {
                    self.delegate?.createEventViewController(self, didFinishAdding: event!)
                }
            } else {
                self.delegate?.createEventViewControllerCancel(self)
            }
            
        }
    }
    
    //MARK:- Notification functions
    func scheduleNotification(uid: String) {
        if date.adding(minutes: -minBefore) > Date() {
            let content = UNMutableNotificationContent()
            content.title = "Reminder:"
            content.body = nameTextField.text!
            content.sound = UNNotificationSound.default()
            let calendar = Calendar.current
            let myDate = date.adding(minutes: -minBefore)
            let components = calendar.dateComponents([.month, .day, .hour, .minute], from: myDate)
            let trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: false)
            let request = UNNotificationRequest(identifier: uid, content: content, trigger: trigger)
            let center = UNUserNotificationCenter.current()
            center.add(request)
            print("Scheduled: \(request) for eventUID: \(uid)")
        }
    }
    
    func removeNotification(uid: String) {
        let center = UNUserNotificationCenter.current()
        center.removePendingNotificationRequests(withIdentifiers: [uid])
    }
    
    
    //MARK:- Actions
    @IBAction func editTime()
    {
        pickTime.becomeFirstResponder()
    }
    
    @IBAction func editDate() {
        pickDate.becomeFirstResponder()
    }
    
    @IBAction func createEvent() {
        nameTextField.resignFirstResponder()
        descriptionTextView.resignFirstResponder()
        pickDate.resignFirstResponder()
        pickTime.resignFirstResponder()
        if !nameTextField.text!.isEmpty && !descriptionTextView.text!.isEmpty && (fiveMinButton.isSelected || tenMinButton.isSelected || fifteenMinButton.isSelected){
            if date.adding(minutes: -minBefore) <= Date() {
                self.showAlert(title: "Whoops...", message: "Choose another date.")
                return
            }
            if let _ = eventToEdit {
                eventToEdit!.nameEvent = nameTextField.text!
                eventToEdit!.descriptionEvent = descriptionTextView.text!
                if imageButtonView.image != #imageLiteral(resourceName: "pictureImage")
                {
                    eventToEdit!.image = UIImageJPEGRepresentation(imageButtonView.image!, 0.5)
                } else {
                    eventToEdit!.image = nil
                }
                if fiveMinButton.isSelected == true {
                    eventToEdit!.minBefore = 5
                }
                if tenMinButton.isSelected == true {
                    eventToEdit!.minBefore = 10
                }
                if fifteenMinButton.isSelected == true {
                    eventToEdit!.minBefore = 15
                }
                eventToEdit!.locationEvent = address
                if let _ = latitude {
                    eventToEdit!.latitude = latitude!

                }
                if let _ = longitude {
                    eventToEdit!.longitude = longitude!
                }
                eventToEdit!.dateEvent = date
                removeNotification(uid: eventToEdit!.uid!)
                scheduleNotification(uid: eventToEdit!.uid!)
                HSCoreDataController.sharedInstance.saveContext()
                WebService.sharedInstance.saveReminder(event: eventToEdit!) { (response) in
                    guard response.isSuccess else {
                        self.showAlert(title: "Atention", message: response.errorMessage!)
                        return
                    }
                    self.withoutContaint(event: self.eventToEdit!)
                }
            } else {
                let uid = UUID().uuidString
                var data:Data?
                if imageButtonView.image != #imageLiteral(resourceName: "pictureImage")
                {
                    data = UIImageJPEGRepresentation(imageButtonView.image!, 0.5)
                }
                var minBefore = 0
                if fiveMinButton.isSelected == true {
                    minBefore = 5
                }
                if tenMinButton.isSelected == true {
                    minBefore = 10
                }
                if fifteenMinButton.isSelected == true {
                    minBefore = 15
                }
                let event = HSCoreDataController.sharedInstance.createOrReplaceEvent(date: date, description: descriptionTextView!.text, image: data, location: address, minBefore: minBefore, name: nameTextField!.text, uid: uid)
                if let _ = latitude {
                    event.latitude = latitude!
                    
                }
                if let _ = longitude {
                    event.longitude = longitude!
                }
                scheduleNotification(uid: uid)
                HSCoreDataController.sharedInstance.saveContext()
                WebService.sharedInstance.saveReminder(event: event) { (response) in
                    guard response.isSuccess else {
                        self.showAlert(title: "Atention", message: response.errorMessage!)
                        return
                    }
                    self.withoutContaint(event: event)
                }
            }
        } else {
            self.showAlert(title: "Whoops...", message: "Complete name, description and alarm!")
        }
    }
    
    @IBAction func choosePhoto() {
        hideKeyboard()
        showPhotoMenu()
    }

    @IBAction func pickLocation() {
        hideKeyboard()
        if let _ = eventToEdit?.locationEvent {
            showMenu()
        } else if let _ = address {
            showMenu()
        } else {
            addLocation()
        }
    }
    
    @IBAction func fiveMinClick() {
        if fiveMinButton.isSelected != true {
            UIView.animate(withDuration: 0.2, animations: {
                self.fiveImageSelectedView.alpha = 1
                if self.tenMinButton.isSelected {
                    self.tenImageSelectedView.alpha = 0
                } else if self.fifteenMinButton.isSelected {
                    self.fifteenSelectedImageView.alpha = 0
                }
            }) { (_) in
                self.fiveMinButton.isSelected = true
                self.tenMinButton.isSelected = false
                self.fifteenMinButton.isSelected = false
                self.minBefore = 5
            }
        } else {
            UIView.animate(withDuration: 0.2, animations: {
                self.fiveImageSelectedView.alpha = 0
            }) { (_) in
                self.fiveMinButton.isSelected = false
                self.minBefore = 0
            }
        }
    }
    
    @IBAction func tenMinClick() {
        if tenMinButton.isSelected != true {
            UIView.animate(withDuration: 0.2, animations: {
                self.tenImageSelectedView.alpha = 1
                if self.fiveMinButton.isSelected {
                    self.fiveImageSelectedView.alpha = 0
                } else if self.fifteenMinButton.isSelected {
                    self.fifteenSelectedImageView.alpha = 0
                }
            }) { (_) in
                self.fiveMinButton.isSelected = false
                self.tenMinButton.isSelected = true
                self.fifteenMinButton.isSelected = false
                self.minBefore = 10
            }
        } else {
            UIView.animate(withDuration: 0.2, animations: {
                self.tenImageSelectedView.alpha = 0
            }) { (_) in
                self.tenMinButton.isSelected = false
                self.minBefore = 0
            }
        }
    }
    
    @IBAction func fifteenMinClick() {
        if fifteenMinButton.isSelected != true {
            UIView.animate(withDuration: 0.2, animations: {
                self.fifteenSelectedImageView.alpha = 1
                if self.fiveMinButton.isSelected {
                    self.fiveImageSelectedView.alpha = 0
                } else if self.tenMinButton.isSelected {
                    self.tenImageSelectedView.alpha = 0
                }
            }) { (_) in
                self.fiveMinButton.isSelected = false
                self.tenMinButton.isSelected = false
                self.fifteenMinButton.isSelected = true
                self.minBefore = 15
            }
        } else {
            UIView.animate(withDuration: 0.2, animations: {
                self.fifteenSelectedImageView.alpha = 0
            }) { (_) in
                self.fifteenMinButton.isSelected = false
                self.minBefore = 0
            }
        }
    }
    
    //MARK:- Pick location menu
    func showMenu() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let deleteLocation = UIAlertAction(title: "Delete location", style: .default, handler: {_ in
            self.address = nil
            self.addLocationButton.setBackgroundImage(#imageLiteral(resourceName: "addLocation"), for: .normal)
        })
        let pickAnAddress = UIAlertAction(title: "Pick a location", style: .default, handler: {_ in self.addLocation()})
        alert.addAction(cancel)
        alert.addAction(deleteLocation)
        alert.addAction(pickAnAddress)
        present(alert, animated: true, completion: nil)
    }
}

extension CreateEventViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return (touch.view === self.view)
    }
}

extension CreateEventViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func takePhotoWithCamera() {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .camera
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
    
    func photoFromLibrary() {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        image = info[UIImagePickerControllerEditedImage] as? UIImage
        if let myImage = image {
            let newImage = myImage
            imageButtonView.image = newImage
        }
        backFromPickImage = true
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        backFromPickImage = true
        dismiss(animated: true, completion: nil)
    }
    
    func showPhotoMenu() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let photoLibrary = UIAlertAction(title: "Choose from library", style: .default, handler: {_ in self.photoFromLibrary()})
        let deletePhoto = UIAlertAction(title: "Delete photo", style: .default, handler: { _ in
            self.image = nil
            self.imageButtonView.image = #imageLiteral(resourceName: "pictureImage")
        })
        alert.addAction(cancel)
        if self.imageButtonView.image != #imageLiteral(resourceName: "pictureImage") {
            alert.addAction(deletePhoto)
        }
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let photoCamera = UIAlertAction(title: "Take photo with camera", style: .default, handler: {_ in self.takePhotoWithCamera()})
            alert.addAction(photoCamera)
        }
        alert.addAction(photoLibrary)
        present(alert, animated: true, completion: nil)
    }
}

extension CreateEventViewController: PickLocationViewControllerDelegate {
    func pickLocationViewController(_ controller: PickLocationViewController, didFinishAdding placemark: CLPlacemark?, latitude: Double?, longitude: Double?) {
        if let _ = placemark {
            var place = ""
                if let _ = placemark!.subThoroughfare {
                    place += placemark!.subThoroughfare!
            }
            if let _ = placemark!.thoroughfare {
                place += " " + placemark!.thoroughfare!
            }
            if let _ = placemark!.locality {
                place += ", " + placemark!.locality!
            }
            if let _ = placemark!.administrativeArea {
                place += ", " + placemark!.administrativeArea!
            }
            if let _ = placemark!.postalCode {
                place += ", " + placemark!.postalCode!
            }
            if let _ = placemark!.country {
                place += ", " + placemark!.country!
            }
            self.latitude = latitude
            self.longitude = longitude
            address = place
            addLocationButton.setBackgroundImage(#imageLiteral(resourceName: "locationDone"), for: .normal)
        } else {
            addLocationButton.setBackgroundImage(#imageLiteral(resourceName: "addLocation"), for: .normal)
            address = nil
        }
    }
    
    
    
}

extension CreateEventViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let existingLines = textView.text.components(separatedBy: CharacterSet.newlines)
        let newLines = text.components(separatedBy: CharacterSet.newlines)
        let linesAfterChange = existingLines.count + newLines.count - 1
        
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        if linesAfterChange != 5
        {
            let lines = newText.components(separatedBy: CharacterSet.newlines)
            let numberOfChars = (lines.count - 1) * 37 + lines.last!.count
            print(lines)
            print("\n")
            if linesAfterChange <= 4 && numberOfChars <= 120 {
                return true
            }
        }
        return false
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let existingLines = textView.text.components(separatedBy: CharacterSet.newlines)
        
        let linesAfterChange = existingLines.count - 1
        
        let number = (existingLines.count - 1) * 37 + existingLines.last!.count

        if linesAfterChange <= 4 && number <= 120
        {
            charactersDescription.text = "\(120 - number)/120 characters"
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == pickDate {
            pickDate.resignFirstResponder()
            var components = Calendar.current.dateComponents([.month, .day, .year, .hour, .minute], from: datePicker.date)
            var time = Calendar.current.dateComponents([.hour, .minute], from: timePicker.date)
            components.hour = time.hour
            components.minute = time.minute
            date = Calendar.current.date(from: components)!
            if let _ = initialDate {
                var componentsInitial = Calendar.current.dateComponents([.month, .day, .year, .hour, .minute], from: initialDate!)
                 if components.day == componentsInitial.day && componentsInitial.month == components.month && componentsInitial.year == components.year {
                    timePicker.minimumDate = initialDate!
                    var timeCurrent = Calendar.current.dateComponents([.month, .day, .year, .hour, .minute], from: timePicker.date)
                    if timeCurrent.hour! <= componentsInitial.hour! &&  timeCurrent.minute! < componentsInitial.minute! {
                        timePicker.date = initialDate!
                        time = Calendar.current.dateComponents([.hour, .minute], from: timePicker.date)
                        components.hour = time.hour
                        components.minute = time.minute
                        date = Calendar.current.date(from: components)!
                    } else {
                        timeCurrent.month = componentsInitial.month
                        timeCurrent.day = componentsInitial.day
                        timeCurrent.year = componentsInitial.year
                        let date = Calendar.current.date(from: timeCurrent)!
                        timePicker.date = date
                    }
                 } else if date > initialDate! {
                    timePicker.minimumDate = nil
                 } else {
                    timePicker.minimumDate = initialDate!
                    if timePicker.date < initialDate! {
                        timePicker.date = initialDate!
                        time = Calendar.current.dateComponents([.hour, .minute], from: timePicker.date)
                        components.hour = time.hour
                        components.minute = time.minute
                        date = Calendar.current.date(from: components)!
                    }
                }
            }
            updateDateLabel()
            updateHourLabel()
        } else if textView == pickTime {
            pickTime.resignFirstResponder()
            var components = Calendar.current.dateComponents([.month, .day, .year, .hour, .minute], from: datePicker.date)
            let time = Calendar.current.dateComponents([.hour, .minute], from: timePicker.date)
            components.hour = time.hour
            components.minute = time.minute
            date = Calendar.current.date(from: components)!
            updateDateLabel()
            updateHourLabel()
        } else if textView == descriptionTextView {
            descriptionTextView.resignFirstResponder()
        }
    }
}

extension CreateEventViewController: UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension Date {
    func adding(minutes: Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: minutes, to: self)!
    }
}
