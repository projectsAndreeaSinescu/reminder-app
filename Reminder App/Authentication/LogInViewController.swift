//
//  LogInViewController.swift
//  Remainder App
//
//  Created by Andreea Sinescu on 10/08/2018.
//  Copyright © 2018 Andreea Sinescu. All rights reserved.
//

import UIKit

class LogInViewController: UIViewController {

    @IBOutlet weak var emailTextField: HSTextField!
    @IBOutlet weak var passwordTextField: HSTextField!
    
    static func createViewController() -> LogInViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
        return controller
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func logIn(_ sender: Any) {
        guard self.emailTextField.validateFieldValue() else {
            return
        }
        
        guard self.passwordTextField.validateFieldValue() else {
            return
        }
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        WebService.sharedInstance.login(email: emailTextField.text!, password: passwordTextField.text!) { (response) in
            guard response.isSuccess else {
                self.showTryAgain(title: "Atention", message: response.errorMessage!)
                return
            }
            let controller = ReminderViewController.createViewController()
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    @IBAction func register(_ sender: Any) {
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        let controller = RegisterViewController.createViewController()
        self.present(controller, animated: true, completion: nil)
    }
    
    func loadData() {
        WebService.sharedInstance.apiSettings { (response) in
            guard response.isSuccess else {
                self.showTryAgain(title: "Try again", message: response.errorMessage!)
                return
            }
        }
    }
    
    func showTryAgain(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Retry", style: .default) { (_) in
            self.loadData()
        }
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }

}
