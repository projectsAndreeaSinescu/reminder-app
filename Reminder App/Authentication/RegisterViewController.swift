//
//  RegisterViewController.swift
//  Remainder App
//
//  Created by Andreea Sinescu on 10/08/2018.
//  Copyright © 2018 Andreea Sinescu. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var emailTextField: HSTextField!
    @IBOutlet weak var passwordTextField: HSTextField!
    @IBOutlet weak var passwordAgainTextField: HSTextField!
    @IBOutlet weak var usernameTextField: HSTextField!
    
    var mediaPicker: HSMediaPicker?
    
    static func createViewController() -> RegisterViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        return controller
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func done(_ sender: Any) {
        guard self.usernameTextField.validateFieldValue() else
        {
            return
        }
        
        guard self.emailTextField.validateFieldValue() else
        {
            return
        }
        guard self.passwordTextField.validateFieldValue() else
        {
            return
        }
        
        guard self.passwordAgainTextField.validateFieldValue() else
        {
            return
        }
        
        let password = passwordTextField.text
        let passwordAgain = passwordAgainTextField.text
        
        guard password == passwordAgain else {
            self.passwordTextField.invalidFieldValue()
            self.passwordAgainTextField.invalidFieldValue()
            self.showAlert(title: "Password", message: "Passwords don't match")
            return
        }
        usernameTextField.resignFirstResponder()
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        passwordAgainTextField.resignFirstResponder()
        WebService.sharedInstance.register(email: emailTextField.text!, password: password!, username: usernameTextField.text!, completion: { (response) in
            guard response.isSuccess else {
                self.showAlert(title: "Atention", message: response.errorMessage!)
                return
            }
            self.dismiss(animated: true, completion: {
                self.showAlert(title: "Register success!", message: "You have an account!")
            })
        } )
    }
    
    @IBAction func back() {
        usernameTextField.resignFirstResponder()
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        passwordAgainTextField.resignFirstResponder()
        dismiss(animated: true, completion: nil)
    }
    

}
