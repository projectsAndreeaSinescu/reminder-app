//
//  HSUtils.swift
//  UberClone
//
//  Created by Lazar Mirela on 20/08/16.
//  Copyright © 2016 Lazar Mirela. All rights reserved.
//

import UIKit

let kHSPrintLogs = true

class HSUtils: NSObject {
    
    static func isSimulator() -> Bool
    {
        #if (arch(i386) || arch(x86_64)) && os(iOS)
            return true
        #else
            return false
        #endif

    }
    
    static func isDeveloper() -> Bool
    {
        return true;
    }
    
    static func validateEmail(_ testStr: String?) -> Bool {
        
        guard testStr != nil else
        {
            return false
        }
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
    static func stringWithUUID() -> NSString {
        
        let uuidObj = CFUUIDCreate(nil)
        let uuidString = CFUUIDCreateString(nil, uuidObj)
        return uuidString!
    }

    static func getDouble(obj: AnyObject) -> Double
    {
        if obj as? Double != nil
        {
            return obj as! Double
        }
        else if obj as? String != nil
        {
            return Double(obj as! String)!
        }
        
        return Double(0)
    }
    
    
    
    static func getNumber(obj: AnyObject) -> NSNumber
    {
        if obj as? NSNumber != nil
        {
            return obj as! NSNumber
        }
        else
        {
            return NSNumber(value: getDouble(obj: obj))
        }
    }
    
    static func convertToLocalDateFromDate(_ date: Date) -> Date {
        let tz = NSTimeZone.local
        let seconds = tz.secondsFromGMT()
        let date = date.addingTimeInterval(TimeInterval(seconds))
        return date
    }
    
    static func getLocalDateFromISOString(_ string: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        var date = dateFormatter.date(from: string)!
        let tz = NSTimeZone.local
        let seconds = tz.secondsFromGMT(for: date)
        date = date.addingTimeInterval((TimeInterval(seconds)))
        return date
        
    }
    
    static func formatFloat(_ obj: Float) -> String
    {
        return NSString(format: "%.2f",obj) as String
    }
    
    static func hsPrint(message: String)
    {
        if (kHSPrintLogs)
        {
            print(message);
        }
    }
    
    //MARK: File management
    static func bundleURL(filename : String) -> URL?
    {
        let nsString : NSString = filename as NSString
        let url = Bundle.main.url(forResource: nsString.deletingPathExtension, withExtension: nsString.pathExtension)
        return url
    }

    
    static func documentsDirectory() -> URL
    {
        return FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory
            , in: FileManager.SearchPathDomainMask.userDomainMask).last!
    }
    
    static func documentsDirectory(path : String) -> URL
    {
        return HSUtils.documentsDirectory().appendingPathComponent(path)
    }
    
    static func checkOrCreateDirectory(path : String) -> Bool
    {
        do
        {

        try FileManager.default.createDirectory(at: HSUtils.documentsDirectory(path: path),
                                                    withIntermediateDirectories: true,
                                                    attributes: nil)
            return true
        }
        catch
        {
            HSLogManager.staticInstance.writeToLogs("Failed to create dir:\(path) error:\(error)")
            return false
        }
    }
    
 
    
    static func getURL(_ string: String?) -> URL?
    {
        guard  string != nil else {
            return nil
        }
        
        
        if string!.hasPrefix("http")
        {
            return URL(string: string!)
        }
        
        return URL.init(fileURLWithPath: string!)
    }
    
    
    static func getRangeIndex(_ value: Float, array : [Float]) -> Int
    {
        var position = 0
        for object in array
        {
            if value > object
            {
                return position
            }
            position += 1
        }
        
        return position
    }

    
    static func timeFormat(_ date: Date) -> String
    {
        let format = DateFormatter()
        format.timeStyle = .short
        format.dateStyle = .none
        return format.string(from: date)
    }
    
    static func colorWithHexString (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substring(from: 1)
        }
        
        if (cString.count != 6) {
            return UIColor.gray
        }
        
        let rString = (cString as NSString).substring(to: 2)
        let gString = ((cString as NSString).substring(from: 2) as NSString).substring(to: 2)
        let bString = ((cString as NSString).substring(from: 4) as NSString).substring(to: 2)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)
        
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
}




