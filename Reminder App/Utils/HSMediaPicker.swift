//
//  ImagePickerExtension.swift
//  Image Picker
//
//  Created by Catalin Lazar on 07/02/2018.
//  Copyright © 2018 Catalin Lazar. All rights reserved.
//

import UIKit
import AVFoundation
import MobileCoreServices

enum HSSourceType
{
    case camera
    case photoLibrary
    case both
}

enum HSMediaType: String
{
    case video = "video"
    case image = "image"
    case both = "media"
}

protocol HSMediaPickerDelegate: class
{
    func didCancel()
    func didFinishPickingMediaWithInfo(info: [String : Any])
}

/// Use media pricker to present an image picker
/// Make sure the object doesn't get deinitializate before it answers
class HSMediaPicker: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    private weak var delegate: HSMediaPickerDelegate!
    private var allowEditing = false
    private var controller: UIViewController!
    
    init(controller:UIViewController, delegate: HSMediaPickerDelegate) {
        self.delegate = delegate
        self.controller = controller
    }
    
    
    /// Present an media picker for source "library" or "camera".
    /// Present an alert controller for both souces.
    ///
    ///
    /// - Parameters:
    ///   - controller: view controller that will present the picker
    ///   - delegate: implement the delegate methods to be notified when user canceled, selected image or selected video
    ///   - source: the source from which media should be picked, default is CAMERA
    ///   - media: default is IMAGE, video is with sound
    ///   - allowEditing: allow user to edit media, default is FALSE
    func presentImagePicker(from source: HSSourceType = .camera, media: HSMediaType = .image, allowEditing: Bool = false)
    {
        self.allowEditing = allowEditing
        switch source
        {
        case .camera, .photoLibrary:
            self.chooseFrom(source: source, media: media, allowEditing: allowEditing)
            
        case .both:
            self.chooseFromBoth(source: source, media: media, allowEditing: allowEditing)
        }
    }
    
    
    private func chooseFrom(source: HSSourceType, media: HSMediaType, allowEditing: Bool)
    {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = source == .camera ? .camera : .photoLibrary
        
        switch media
        {
        case .image:
            imagePicker.mediaTypes = [kUTTypeImage as String]
            
        case .video:
            imagePicker.mediaTypes = [kUTTypeMovie as String]
            
        case .both:
            imagePicker.mediaTypes = [kUTTypeImage as String, kUTTypeMovie as String]
        }
        
        imagePicker.allowsEditing = allowEditing
        controller.present(imagePicker, animated: true, completion: nil)
    }
    
    private func chooseFromBoth(source: HSSourceType,  media: HSMediaType, allowEditing: Bool)
    {
         let alert = UIAlertController(title: "Add \(media.rawValue)", message: "Select your \(media.rawValue) input source.", preferredStyle: .actionSheet)
        let actionCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let actionLibrary = UIAlertAction(title: "Library", style: .default) { (_) in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary)
            {
                self.chooseFrom(source: .photoLibrary, media: media, allowEditing: allowEditing)
            }
        }
        let actionCamera = UIAlertAction(title: "Camera", style: .default) { (_) in
            if UIImagePickerController.isSourceTypeAvailable(.camera)
            {
                self.chooseFrom(source: .camera, media: media, allowEditing: allowEditing)
            }
        }
        
        alert.addAction(actionCamera)
        alert.addAction(actionCancel)
        alert.addAction(actionLibrary)
        
        controller.present(alert, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        guard delegate != nil else { return }
        delegate.didCancel()
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        picker.dismiss(animated: true, completion: nil)
        
        guard delegate != nil else { return }
        delegate.didFinishPickingMediaWithInfo(info: info)
    }
}
