//
//  HSTextFieldView.swift
//  Chroma_maker
//
//  Created by Lazar Mirela on 19/10/16.
//  Copyright © 2016 Lazar Mirela. All rights reserved.
//

import UIKit

/**
 Inherits UITextfield
 Functionality:
 - add textfield underline
 - text left, right insets
 - min character validation
 - max character validation - textfield will not allow user to exceed the limit
 - if the keyboard is of type email - text vaidation includes basic email validation
 - settable from storyboard and xib
 */
//@IBDesignable
class HSTextField: UITextField {

    private var underlineLayer: CALayer?

    /// disable copy, paste and select all
    @IBInspectable var disablaActions: Bool = false
    
    ///border color
    @IBInspectable var hsBorderColor : UIColor?
    {
        didSet
        {
            self.layer.borderColor = hsBorderColor?.cgColor
        }
        
    }
    
    ///border Width
    @IBInspectable var hsBorderWidth : Int = 0
    {
        didSet
        {
            self.layer.borderWidth = CGFloat(hsBorderWidth)
        }
        
    }    
    ///border corner
    @IBInspectable var hsBorderCornerRadius : Int = 0
    {
        didSet
        {
            self.layer.cornerRadius = CGFloat(hsBorderCornerRadius)
        }
    }
    ///underline height
    @IBInspectable var hsUnderlineHeight : Int = 2
    ///textinset left and right
    @IBInspectable var hsTextHorizontalEdge : Int = 2
    ///maximum number of characters
    @IBInspectable var hsMaxCharacterCount : Int = 0
    ///minimum number of characters
    @IBInspectable var hsMinCharacterCount : Int = 0
    ///underline coulor
    @IBInspectable var underlineColor : UIColor?
    {
        didSet
        {
            if underlineLayer == nil
            {
                underlineLayer = CALayer()
                underlineLayer?.frame = CGRect(x: CGFloat(0),
                                               y: self.frame.size.height - CGFloat(hsUnderlineHeight),
                                               width:  self.frame.size.width,
                                               height: CGFloat(hsUnderlineHeight))
                
                self.layer.addSublayer(underlineLayer!)
                self.layer.masksToBounds = true
                
            }
            underlineLayer?.backgroundColor = underlineColor?.cgColor
        }
    }

    ///placeholder color
    @IBInspectable var placeholderColor : UIColor?
    {
        didSet
        {
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addTarget(self, action: #selector(limitEditingChanged), for: .editingChanged)
        if (self.placeholder != nil) && (self.font != nil)
        {
            if placeholderColor != nil
            {
                self.attributedPlaceholder = NSAttributedString(string: self.placeholder!,
                                                                attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: self.font!.pointSize), NSAttributedStringKey.foregroundColor : placeholderColor!])
            }
            else
            {
                self.attributedPlaceholder = NSAttributedString(string: self.placeholder!,
                                                                attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: self.font!.pointSize)])
            }
        }
    }

    ///update underline when layout changes
    override func layoutSubviews() {
        if underlineLayer != nil
        {
            underlineLayer?.frame.size.width = self.frame.size.width
            underlineLayer?.frame.origin.y = self.frame.size.height - CGFloat(hsUnderlineHeight)
        }
        
        super.layoutSubviews()

    }
    
    ///add left and right insets
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        var insetBounds = bounds
        insetBounds.origin.x = insetBounds.origin.x + CGFloat(hsTextHorizontalEdge)
        insetBounds.size.width = insetBounds.size.width - CGFloat(hsTextHorizontalEdge)
        return insetBounds
    }
    
    
    ///add left and right insets
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        var insetBounds = bounds
        insetBounds.origin.x = insetBounds.origin.x + CGFloat(hsTextHorizontalEdge)
        insetBounds.size.width = insetBounds.size.width - CGFloat(hsTextHorizontalEdge)
        return insetBounds
    }

    ///limit the number of characters - only if a max character cout value is set and is above 0
    @objc func limitEditingChanged()
    {
        if let text = self.text
        {
            if (self.hsMaxCharacterCount > 0)
            {
                if (text.characters.count > hsMaxCharacterCount)
                {
                    let start = text.startIndex
                    self.text = text.substring(to: text.index(start, offsetBy: hsMaxCharacterCount))
                }
                
            }
        }
        self.validFieldValue(true)

    }
    
    /**
     Validate the current value
        - if character limit is set - check the limit
        - if email keyboard is used - check the text content
        - if invalid: placeholder and underline become red, otherwise original colour is used
        - returns: true if all checks are passed
     */
    func validateFieldValue() -> Bool
    {
        guard  hsMinCharacterCount > 0 else {
            if keyboardType == .emailAddress
            {
                let valid = HSUtils.validateEmail(text)
                self.validFieldValue(valid)
                return valid
            }
            return true
        }
        
        if let text = self.text
        {
            if text.characters.count >= hsMinCharacterCount
            {
                if keyboardType == .emailAddress
                {
                    let valid = HSUtils.validateEmail(text)
                    self.validFieldValue(valid)
                    return valid
                }
                self.validFieldValue(true)
                return true
            }
        }
        
        self.validFieldValue(false)
        return false
    }
    
    ///field is invalid, use in case the field has a combined validation condition(Ex: repeat password)
    func invalidFieldValue()
    {
        self.validFieldValue(false)
    }
    
    /**
     Update UI based on validation
     - if invalid: placeholder and underline become red, otherwise original colour is used
     - parameter valid: text content validation value updates UI configuration
     */
    private func validFieldValue(_ valid: Bool) {
        if valid
        {
            if (underlineLayer != nil)
            {
                underlineLayer?.backgroundColor = underlineColor?.cgColor
            }
            else
            {
                self.layer.borderColor = self.hsBorderColor?.cgColor
            }
            
            if (self.placeholder != nil) && (self.font != nil)
            {
                if placeholderColor != nil
                {
                    self.attributedPlaceholder = NSAttributedString(string: self.placeholder!,
                            attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: self.font!.pointSize), NSAttributedStringKey.foregroundColor : placeholderColor!])
                }
                else
                {
                    self.attributedPlaceholder = NSAttributedString(string: self.placeholder!,
                            attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: self.font!.pointSize)])
                }
            }
        }
        else
        {
            if (underlineLayer != nil)
            {
                underlineLayer?.backgroundColor = UIColor.red.cgColor
            }
            else
            {
                self.layer.borderColor = UIColor.red.cgColor
            }
            if (self.placeholder != nil) && (self.font != nil)
            {
                self.attributedPlaceholder = NSAttributedString(string: self.placeholder!,
                                                                attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: self.font!.pointSize),
                                                                             NSAttributedStringKey.foregroundColor: UIColor.red])
            }
        }
    }
    
    /// disable paste and other actions
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(copy(_:)) || action == #selector(selectAll(_:)) || action == #selector(paste(_:)) {
            
            return disablaActions == false
        }
        return super.canPerformAction(action, withSender: sender)
    }
}
