
//
//  HSRetrieveLocation.swift
//
//  Created by Mirela Lazar on 16/01/2017.
//  Copyright © 2017 Hypersense. All rights reserved.
//

import Foundation
import CoreLocation


/// Retrieve a single location
/// Location is returned in the callback, if there are any errors the callback will return appropiate code
class HSRetrieveLocation: NSObject, CLLocationManagerDelegate
{
    enum ErrorCode: Int  {
        case none = 0
        case permissionDenied = 1
        case locationDisabled = 2
        case noLocation = 3
        
        /// Human readable message
        func message() -> String
        {
            switch self {
            case .none: return "Location was retrieved"
            case .permissionDenied: return "Permission denied, please enable location permission from phone settings"
            case .locationDisabled: return "Functionality is disabled"
            case .noLocation: return "No valid location was detected"
            }
        }

    }
    
    /// Location completion block
    /// - Parameter error - error code
    /// - Parameter location - a valid location
    typealias locationCompletionBlock = (_ error : ErrorCode, _ location : CLLocation?) -> Void
    
    private var locationManager : CLLocationManager
    private var bestLocation : CLLocation?
    private var completionBlock : locationCompletionBlock?
    
    /// max detection time - unless the desired accuracy is achieved. Do not alter after requesting the location. Default 60
    var maxLocationTime : TimeInterval = 60
    /// max location age - if older locations are retrieved, they are invalid. Do not alter after requesting the location. Default 60
    var maxLocationAge : TimeInterval = 60
    /// min accuracy - stop locating if a location with better accuracy is received. Do not alter after requesting the location. Default 65
    var minLocationAccuracy : Double = 65
    
    
    /// Init using a completion block. Default values are used for minLocationAccuracy
    ///
    /// - Parameters:
    ///   - maxLocationAge: ignore locations older that the given limit
    ///   - maxLocateTime: max time spec=nt to fnd a location, if a location that fits minLocationAccuracy is found manager will stop before reaching the time limit
    ///   - completionBlock: completion block to be called once after a location is found or when timer expires
    init(maxLocationAge : TimeInterval = 60,
         maxLocateTime: TimeInterval = 60,
         minLocationAccuracy: Double = 65,
         completionBlock: locationCompletionBlock!) {
        
        self.locationManager = CLLocationManager()
        
        super.init()
        
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        self.locationManager.activityType = .fitness
        self.locationManager.pausesLocationUpdatesAutomatically = false
        
        if #available(iOS 9.0, *) {
            self.locationManager.allowsBackgroundLocationUpdates = false
        } else {
            self.locationManager.requestAlwaysAuthorization()
        }
        self.locationManager.delegate = self
        self.completionBlock = completionBlock
        self.maxLocationTime = maxLocateTime
        self.maxLocationAge = maxLocationAge
        self.minLocationAccuracy = minLocationAccuracy
    }
    
    
    
    /// Start detecting location
    func startLocationUpdate() {
        if CLLocationManager.locationServicesEnabled() == false {
            print("\(#function)--->location disabled")
            complete(error: .locationDisabled)
            
        }
        else {
            let authorizationStatus = CLLocationManager.authorizationStatus()
            if authorizationStatus == .denied || authorizationStatus == .restricted {
                complete(error: .permissionDenied)
            }
            else {
                print("\(#function)--->Stop after %.2f sec")
                locationManager.requestWhenInUseAuthorization()
                self.perform(#selector(stopLocationUpdate), with: nil, afterDelay: maxLocationTime)
                locationManager.startUpdatingLocation()
            }
        }
    }
    
    /// Force stop detecting location
    /// Warning: Will call completion block
    @objc func stopLocationUpdate() {
        self.locationManager.delegate = nil
        locationManager.stopUpdatingLocation()
        
        self.complete(location: bestLocation)
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(stopLocationUpdate), object: nil)
    }
    
    // MARK: - location delegate
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.stopLocationUpdate()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        var mostAccurateLocation: CLLocation? = nil
        if (bestLocation != nil) && (-(bestLocation!.timestamp.timeIntervalSinceNow) < maxLocationAge) {
            mostAccurateLocation = bestLocation
        }
        for i in 0..<locations.count {
            let newLocation = locations[i]
            let theLocation = newLocation.coordinate
            let theAccuracy = newLocation.horizontalAccuracy
            let locationAge = -newLocation.timestamp.timeIntervalSinceNow
            
            if (locationAge < maxLocationAge) && (theAccuracy > 0) {
                //Select only valid location and also location with good accuracy
                if theAccuracy > 0 &&
                    (!(mostAccurateLocation != nil) || (mostAccurateLocation!.horizontalAccuracy >= theAccuracy)) &&
                    (!(theLocation.latitude == 0.0 && theLocation.longitude == 0.0)) {
                    mostAccurateLocation = newLocation
                }
                
            }
        }
        if mostAccurateLocation != nil {
            bestLocation = mostAccurateLocation
            if bestLocation!.horizontalAccuracy <= minLocationAccuracy {
                self.stopLocationUpdate()
            }
        }
    }
    
    
    private func complete(error: ErrorCode)
    {
        guard self.completionBlock != nil else {
            return
        }
        self.completionBlock?(error, nil)
        self.completionBlock = nil
        
    }
    
    private func complete(location: CLLocation?)
    {
        guard self.completionBlock != nil else {
            return
        }
        
        guard location != nil else {
            self.completionBlock?(.noLocation, nil)
            self.completionBlock = nil
            return
        }
        
        guard CLLocationCoordinate2DIsValid(location!.coordinate) else {
            self.completionBlock?(.noLocation, nil)
            self.completionBlock = nil
            return
        }
        self.completionBlock?(.none, location)
        self.completionBlock = nil
        
    }
    
}
