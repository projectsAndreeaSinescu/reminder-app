//
//  HSLogManager.swift
//  PursuitAlert
//
//  Created by HyperSense on 19/07/16.
//  Copyright © 2016 HyperSense. All rights reserved.
//

import UIKit

/**
 Debug device logs - Stores logs on daily bases
 - Warning: Singleton class
 */
class HSLogManager: NSObject {
    
    static private var folder: String="LogFiles"
    /**
     Access to static instance
     */
    static var staticInstance = HSLogManager()
    
    private override init() {
        super.init()
    }
    
    /**
     Log file urls
     - returns: the log file URLs
     */
    func logFiles() -> [NSURL]?
    {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        var urlPath = NSURL(fileURLWithPath: documentsPath)
        let logsFolder = urlPath.appendingPathComponent(HSLogManager.folder)
        
        do
        {
            try FileManager.default.createDirectory(atPath: logsFolder!.path, withIntermediateDirectories:true, attributes: nil)
            urlPath = urlPath.appendingPathComponent(HSLogManager.folder)! as NSURL
            let contents = try FileManager.default.contentsOfDirectory(at: urlPath as URL, includingPropertiesForKeys: nil, options: FileManager.DirectoryEnumerationOptions.skipsHiddenFiles)
            
            return contents as [NSURL]
        }
        catch
        {
            print("Error \(error)")
            return nil
        }
    }
    
    /**
     Returns the current day log file
     - returns: log file URL
     */
    func logFile() -> NSURL
    {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        
        var urlPath = NSURL(fileURLWithPath: documentsPath)
        let logsFolder = urlPath.appendingPathComponent(HSLogManager.folder)
        
        do
        {
            try FileManager.default.createDirectory(atPath: logsFolder!.path, withIntermediateDirectories:true, attributes: nil)
            urlPath = urlPath.appendingPathComponent(HSLogManager.folder)! as NSURL
        }
        catch
        {
            print("Error \(error)")
            
        }
        let date = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        let dateString = formatter.string(from: date as Date)
        
        return urlPath.appendingPathComponent("logs-\(dateString).txt")! as NSURL
    }
    
    /**
     Adds a log entery to the current day log file
     - parameter message: message to log
     - warning: message is laso printed to console
     */
    func writeToLogs(_ message: String)
    {
        let urlPath = self.logFile()
        
        do
        {
            let date = NSDate()
            let messageFormat = "\(date)::\(message)\n\n"
            
            if FileManager.default.fileExists(atPath: urlPath.path!) {
                let fileHandle = try FileHandle(forWritingTo: urlPath as URL)
                
                fileHandle.seekToEndOfFile()
                
                if let data = messageFormat.data(using: String.Encoding.utf8)
                {
                    fileHandle.write(data)
                }
                
                fileHandle.closeFile()
                
            }
            else {
                try messageFormat.write(to: urlPath as URL, atomically: true, encoding: String.Encoding.utf8)
            }
            print("print to logs \(messageFormat)")
            
        }
        catch
        {
            print("Failed to write logs \(error)")
        }
        
        
    }
}
