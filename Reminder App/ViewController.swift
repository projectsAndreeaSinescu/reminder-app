//
//  ViewController.swift
//  Remainder App
//
//  Created by Andreea Sinescu on 19/07/2018.
//  Copyright © 2018 Andreea Sinescu. All rights reserved.
//

import UIKit
import BXProgressHUD

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    var currentLoading: CGFloat = 0
    var hud: BXProgressHUD!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        HSCoreDataController.sharedInstance.deleteManagedObject(Event())
        // Dispose of any resources that can be recreated.
    }
    
    func simulateLoading()
    {
        guard currentLoading < 1 else {
            self.hud.hide()
            return }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.currentLoading += 0.1
            self.hud.progress = self.currentLoading
            self.simulateLoading()
        }
    }

//    @IBAction func showHud()
//    {
//        hud = BXProgressHUD.showHUDAddedTo(self.view, animated: true)
//        hud.label.text = "loading"
//        hud.mode = .determinateHorizontalBar
//        currentLoading = 0
//        hud.progress = 0
//        simulateLoading()
//
//    }
    

}

